class LoaderToken {
  final String key;
  final String? tag;

  const LoaderToken(
    this.key, {
    this.tag,
  });

  @override
  String toString() {
    return 'LoaderToken<$key, $tag>';
  }
}
