import 'package:get/get.dart';

import 'loader_token.dart';

class LoadingController extends GetxController {
  static LoadingController get to => Get.find<LoadingController>();

  final List<LoaderToken> _loadings = <LoaderToken>[];
  final List<LoaderToken> _pendingStarts = <LoaderToken>[];
  bool get loading => _loadings.isNotEmpty;

  Future? _delayedStart;

  void startLoading(
    String key, {
    String? tag,
    bool delayed = false,
  }) {
    if (delayed && !loading) {
      _pendingStarts.add(LoaderToken(key, tag: tag));
      _delayedStart ??= Future.delayed(const Duration(seconds: 1)).then((_) {
        _loadings.addAll(_pendingStarts);
        _pendingStarts.clear();
        _delayedStart = null;
        update();
      });
    } else {
      _loadings.add(LoaderToken(key, tag: tag));
      update();
    }
  }

  void endLoading(String key) {
    _pendingStarts.removeWhere((element) => element.key == key);
    _loadings.removeWhere((element) => element.key == key);
    update();
  }

  void endMultiple(String tag) {
    _pendingStarts.removeWhere((element) => element.tag == tag);
    _loadings.removeWhere((element) => element.tag == tag);
    update();
  }

  bool isLoading(String key) {
    try {
      _loadings.firstWhere((element) => element.key == key);
      return true;
    } catch (_) {
      return false;
    }
  }
}
