## 0.4.0

* updated min dart sdk to 3.0.0
* updated min flutter version to 3.10.0

## 0.3.0

* publication on unpub.fortysix.world

## 0.2.0

* updated fs_lints gitlab url

## 0.1.0

* update to flutter 3
* use internal lints

## 0.0.1

* initial loading controller implementation
