import 'fs_widget_localizations.dart';

/// The translations for English (`en`).
class FSWidgetLocalizationsEn extends FSWidgetLocalizations {
  FSWidgetLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String get formFiledRequired => 'Required';

  @override
  String get dateFormFieldInvalidFormat => 'Invalid date format';

  @override
  String get dateFormFieldNow => 'Today';

  @override
  String get dateFromFieldSelect => 'Select date';

  @override
  String get dateFormFieldClear => 'Clear';

  @override
  String get timeFormFieldInvalidFormat => 'Invalid time format';

  @override
  String get timeFormFieldNow => 'Now';

  @override
  String get timeFromFieldSelect => 'Select time';

  @override
  String get timeFormFieldClear => 'Clear';

  @override
  String get timeDurationFormFieldClear => 'Clear';

  @override
  String get idFormFieldNoEntitiesFound => 'No entries found';

  @override
  String get idFormFieldStartTyping => 'Start typing';

  @override
  String get idFormFieldSelectionRequired => 'Choose from the list';

  @override
  String get idFormFieldSearchEntity => 'Search entry';

  @override
  String get passwordFormFieldShow => 'Show';

  @override
  String get passwordFormFieldHide => 'Hide';
}
