import 'fs_widget_localizations.dart';

/// The translations for German (`de`).
class FSWidgetLocalizationsDe extends FSWidgetLocalizations {
  FSWidgetLocalizationsDe([String locale = 'de']) : super(locale);

  @override
  String get formFiledRequired => 'Pflichtfeld';

  @override
  String get dateFormFieldInvalidFormat => 'Ungültiges Datumsformat';

  @override
  String get dateFormFieldNow => 'Heute';

  @override
  String get dateFromFieldSelect => 'Datum auswählen';

  @override
  String get dateFormFieldClear => 'Leeren';

  @override
  String get timeFormFieldInvalidFormat => 'Ungültiges Zeitformat';

  @override
  String get timeFormFieldNow => 'Jetzt';

  @override
  String get timeFromFieldSelect => 'Zeit auswählen';

  @override
  String get timeFormFieldClear => 'Leeren';

  @override
  String get timeDurationFormFieldClear => 'Leeren';

  @override
  String get idFormFieldNoEntitiesFound => 'Keine Einträge gefunden';

  @override
  String get idFormFieldStartTyping => 'Fang an zu tippen';

  @override
  String get idFormFieldSelectionRequired => 'Bitte wähle aus der Liste';

  @override
  String get idFormFieldSearchEntity => 'Eintrag suchen';

  @override
  String get passwordFormFieldShow => 'Anzeigen';

  @override
  String get passwordFormFieldHide => 'Verbergen';
}
