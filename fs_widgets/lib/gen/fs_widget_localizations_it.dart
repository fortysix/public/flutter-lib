import 'fs_widget_localizations.dart';

/// The translations for Italian (`it`).
class FSWidgetLocalizationsIt extends FSWidgetLocalizations {
  FSWidgetLocalizationsIt([String locale = 'it']) : super(locale);

  @override
  String get formFiledRequired => 'Necessario';

  @override
  String get dateFormFieldInvalidFormat => 'Formato data non valido';

  @override
  String get dateFormFieldNow => 'Oggi';

  @override
  String get dateFromFieldSelect => 'Seleziona data';

  @override
  String get dateFormFieldClear => 'Cancellare';

  @override
  String get timeFormFieldInvalidFormat => 'Formato orario non valido';

  @override
  String get timeFormFieldNow => 'Ora';

  @override
  String get timeFromFieldSelect => 'Seleziona ora';

  @override
  String get timeFormFieldClear => 'Cancellare';

  @override
  String get timeDurationFormFieldClear => 'Cancellare';

  @override
  String get idFormFieldNoEntitiesFound => 'Nessuna voce trovata';

  @override
  String get idFormFieldStartTyping => 'Inizia a scrivere';

  @override
  String get idFormFieldSelectionRequired => 'Scegliere dall\'elenco';

  @override
  String get idFormFieldSearchEntity => 'Cerca voce';

  @override
  String get passwordFormFieldShow => 'Visualizzare';

  @override
  String get passwordFormFieldHide => 'Mascherare';
}
