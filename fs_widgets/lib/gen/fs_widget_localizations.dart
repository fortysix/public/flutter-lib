import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart' as intl;

import 'fs_widget_localizations_de.dart';
import 'fs_widget_localizations_en.dart';
import 'fs_widget_localizations_it.dart';

/// Callers can lookup localized strings with an instance of FSWidgetLocalizations
/// returned by `FSWidgetLocalizations.of(context)`.
///
/// Applications need to include `FSWidgetLocalizations.delegate()` in their app's
/// `localizationDelegates` list, and the locales they support in the app's
/// `supportedLocales` list. For example:
///
/// ```dart
/// import 'gen/fs_widget_localizations.dart';
///
/// return MaterialApp(
///   localizationsDelegates: FSWidgetLocalizations.localizationsDelegates,
///   supportedLocales: FSWidgetLocalizations.supportedLocales,
///   home: MyApplicationHome(),
/// );
/// ```
///
/// ## Update pubspec.yaml
///
/// Please make sure to update your pubspec.yaml to include the following
/// packages:
///
/// ```yaml
/// dependencies:
///   # Internationalization support.
///   flutter_localizations:
///     sdk: flutter
///   intl: any # Use the pinned version from flutter_localizations
///
///   # Rest of dependencies
/// ```
///
/// ## iOS Applications
///
/// iOS applications define key application metadata, including supported
/// locales, in an Info.plist file that is built into the application bundle.
/// To configure the locales supported by your app, you’ll need to edit this
/// file.
///
/// First, open your project’s ios/Runner.xcworkspace Xcode workspace file.
/// Then, in the Project Navigator, open the Info.plist file under the Runner
/// project’s Runner folder.
///
/// Next, select the Information Property List item, select Add Item from the
/// Editor menu, then select Localizations from the pop-up menu.
///
/// Select and expand the newly-created Localizations item then, for each
/// locale your application supports, add a new item and select the locale
/// you wish to add from the pop-up menu in the Value field. This list should
/// be consistent with the languages listed in the FSWidgetLocalizations.supportedLocales
/// property.
abstract class FSWidgetLocalizations {
  FSWidgetLocalizations(String locale) : localeName = intl.Intl.canonicalizedLocale(locale.toString());

  final String localeName;

  static FSWidgetLocalizations? of(BuildContext context) {
    return Localizations.of<FSWidgetLocalizations>(context, FSWidgetLocalizations);
  }

  static const LocalizationsDelegate<FSWidgetLocalizations> delegate = _FSWidgetLocalizationsDelegate();

  /// A list of this localizations delegate along with the default localizations
  /// delegates.
  ///
  /// Returns a list of localizations delegates containing this delegate along with
  /// GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate,
  /// and GlobalWidgetsLocalizations.delegate.
  ///
  /// Additional delegates can be added by appending to this list in
  /// MaterialApp. This list does not have to be used at all if a custom list
  /// of delegates is preferred or required.
  static const List<LocalizationsDelegate<dynamic>> localizationsDelegates = <LocalizationsDelegate<dynamic>>[
    delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  /// A list of this localizations delegate's supported locales.
  static const List<Locale> supportedLocales = <Locale>[
    Locale('de'),
    Locale('en'),
    Locale('it')
  ];

  /// No description provided for @formFiledRequired.
  ///
  /// In de, this message translates to:
  /// **'Pflichtfeld'**
  String get formFiledRequired;

  /// No description provided for @dateFormFieldInvalidFormat.
  ///
  /// In de, this message translates to:
  /// **'Ungültiges Datumsformat'**
  String get dateFormFieldInvalidFormat;

  /// No description provided for @dateFormFieldNow.
  ///
  /// In de, this message translates to:
  /// **'Heute'**
  String get dateFormFieldNow;

  /// No description provided for @dateFromFieldSelect.
  ///
  /// In de, this message translates to:
  /// **'Datum auswählen'**
  String get dateFromFieldSelect;

  /// No description provided for @dateFormFieldClear.
  ///
  /// In de, this message translates to:
  /// **'Leeren'**
  String get dateFormFieldClear;

  /// No description provided for @timeFormFieldInvalidFormat.
  ///
  /// In de, this message translates to:
  /// **'Ungültiges Zeitformat'**
  String get timeFormFieldInvalidFormat;

  /// No description provided for @timeFormFieldNow.
  ///
  /// In de, this message translates to:
  /// **'Jetzt'**
  String get timeFormFieldNow;

  /// No description provided for @timeFromFieldSelect.
  ///
  /// In de, this message translates to:
  /// **'Zeit auswählen'**
  String get timeFromFieldSelect;

  /// No description provided for @timeFormFieldClear.
  ///
  /// In de, this message translates to:
  /// **'Leeren'**
  String get timeFormFieldClear;

  /// No description provided for @timeDurationFormFieldClear.
  ///
  /// In de, this message translates to:
  /// **'Leeren'**
  String get timeDurationFormFieldClear;

  /// No description provided for @idFormFieldNoEntitiesFound.
  ///
  /// In de, this message translates to:
  /// **'Keine Einträge gefunden'**
  String get idFormFieldNoEntitiesFound;

  /// No description provided for @idFormFieldStartTyping.
  ///
  /// In de, this message translates to:
  /// **'Fang an zu tippen'**
  String get idFormFieldStartTyping;

  /// No description provided for @idFormFieldSelectionRequired.
  ///
  /// In de, this message translates to:
  /// **'Bitte wähle aus der Liste'**
  String get idFormFieldSelectionRequired;

  /// No description provided for @idFormFieldSearchEntity.
  ///
  /// In de, this message translates to:
  /// **'Eintrag suchen'**
  String get idFormFieldSearchEntity;

  /// No description provided for @passwordFormFieldShow.
  ///
  /// In de, this message translates to:
  /// **'Anzeigen'**
  String get passwordFormFieldShow;

  /// No description provided for @passwordFormFieldHide.
  ///
  /// In de, this message translates to:
  /// **'Verbergen'**
  String get passwordFormFieldHide;
}

class _FSWidgetLocalizationsDelegate extends LocalizationsDelegate<FSWidgetLocalizations> {
  const _FSWidgetLocalizationsDelegate();

  @override
  Future<FSWidgetLocalizations> load(Locale locale) {
    return SynchronousFuture<FSWidgetLocalizations>(lookupFSWidgetLocalizations(locale));
  }

  @override
  bool isSupported(Locale locale) => <String>['de', 'en', 'it'].contains(locale.languageCode);

  @override
  bool shouldReload(_FSWidgetLocalizationsDelegate old) => false;
}

FSWidgetLocalizations lookupFSWidgetLocalizations(Locale locale) {


  // Lookup logic when only language code is specified.
  switch (locale.languageCode) {
    case 'de': return FSWidgetLocalizationsDe();
    case 'en': return FSWidgetLocalizationsEn();
    case 'it': return FSWidgetLocalizationsIt();
  }

  throw FlutterError(
    'FSWidgetLocalizations.delegate failed to load unsupported locale "$locale". This is likely '
    'an issue with the localizations generation tool. Please file an issue '
    'on GitHub with a reproducible sample app and the gen-l10n configuration '
    'that was used.'
  );
}
