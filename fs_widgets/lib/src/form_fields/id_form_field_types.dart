typedef EntityToIntIdFunction<T> = int Function(T entity);
typedef EntityToStringIdFunction<T> = String Function(T entity);
typedef EntityToLabelFunction<T> = String Function(T entity);
typedef IntIdToEntityFunction<T> = Future<T?> Function(int id);
typedef StringIdToEntityFunction<T> = Future<T?> Function(String id);
typedef QueryEntityFunction<T> = Future<List<T>> Function(String? query);
typedef SelectEntityFunction<T> = Future<T?> Function();
