import 'package:flutter/material.dart';

import '../../gen/fs_widget_localizations.dart';
import '../controller/string_controller.dart';

class FSPasswordInputField extends StatefulWidget {
  const FSPasswordInputField({
    super.key,
    this.controller,
    this.decoration,
    this.autovalidateMode,
    this.enabled = true,
    this.onFieldSubmitted,
    this.autofocus = false,
  });

  final FSStringInputController? controller;
  final InputDecoration? decoration;
  final AutovalidateMode? autovalidateMode;
  final bool enabled;
  final ValueChanged<String>? onFieldSubmitted;
  final bool autofocus;

  @override
  State<FSPasswordInputField> createState() => _FSPasswordInputFieldState();
}

class _FSPasswordInputFieldState extends State<FSPasswordInputField> {
  late final TextEditingController _textInputController;
  late final FSStringInputController _localController;

  FSStringInputController get _actualController =>
      widget.controller ?? _localController;

  bool _hidePassword = true;

  @override
  void initState() {
    super.initState();

    _textInputController =
        TextEditingController(text: widget.controller?.value);
    _localController = FSStringInputController(_textInputController.text);

    _textInputController.addListener(_onTextChanged);
    _actualController.addListener(_onValueChanged);
  }

  @override
  void dispose() {
    _actualController.removeListener(_onValueChanged);
    _textInputController.removeListener(_onTextChanged);

    _localController.dispose();
    _textInputController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final localizations = FSWidgetLocalizations.of(context)!;
    var actualDecoration = widget.decoration ?? const InputDecoration();
    actualDecoration = actualDecoration.copyWith(
      suffixIcon: Tooltip(
        message: _hidePassword
            ? localizations.passwordFormFieldShow
            : localizations.passwordFormFieldHide,
        child: IconButton(
          onPressed: () {
            setState(() {
              _hidePassword = !_hidePassword;
            });
          },
          icon: _hidePassword
              ? const Icon(Icons.visibility_outlined)
              : const Icon(Icons.visibility_off_outlined),
        ),
      ),
    );

    return TextFormField(
      decoration: actualDecoration,
      controller: _textInputController,
      enabled: widget.enabled,
      readOnly: !widget.enabled,
      validator: _actualController.validator,
      autovalidateMode: widget.autovalidateMode,
      obscureText: _hidePassword,
      onFieldSubmitted: widget.onFieldSubmitted,
      autofocus: widget.autofocus,
    );
  }

  void _onValueChanged() {
    _textInputController.value =
        _textInputController.value.copyWith(text: _actualController.value);
  }

  void _onTextChanged() {
    _actualController.value = _textInputController.text;
  }
}
