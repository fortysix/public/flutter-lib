import 'package:flutter/material.dart';

import '../controller/bool_controller.dart';

class FSCheckboxInputField extends StatefulWidget {
  const FSCheckboxInputField({
    super.key,
    this.controller,
    this.decoration,
    this.enabled = true,
    this.autofocus = false,
  });

  final FSBoolInputController? controller;
  final InputDecoration? decoration;
  final bool enabled;
  final bool autofocus;

  @override
  State<FSCheckboxInputField> createState() => _FSCheckboxInputFieldState();
}

class _FSCheckboxInputFieldState extends State<FSCheckboxInputField> {
  late final FSBoolInputController _localController;

  FSBoolInputController get _actualController =>
      widget.controller ?? _localController;

  @override
  void initState() {
    super.initState();

    _localController = FSBoolInputController(widget.controller?.value);

    _actualController.addListener(_onValueChanged);
  }

  @override
  void dispose() {
    _actualController.removeListener(_onValueChanged);

    _localController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var actualLabel = widget.decoration?.label;
    if (actualLabel == null && widget.decoration?.labelText != null) {
      actualLabel = Text(
        widget.decoration!.labelText!,
        style: widget.decoration!.labelStyle,
      );
    }
    return CheckboxListTile(
      title: actualLabel,
      value: _actualController.value ?? false,
      onChanged: (value) => _actualController.value = value,
      enabled: widget.enabled,
      controlAffinity: ListTileControlAffinity.leading,
      autofocus: widget.autofocus,
    );
  }

  void _onValueChanged() {
    if (mounted) {
      setState(() {});
    }
  }
}
