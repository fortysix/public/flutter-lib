import 'dart:async';
import 'dart:math';

import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../gen/fs_widget_localizations.dart';
import '../controller/string_id_controller.dart';
import 'id_form_field_types.dart';

class FSStringIdInputField<T> extends StatefulWidget {
  const FSStringIdInputField({
    required this.entityToId,
    required this.entityToLabel,
    required this.fetchEntity,
    required this.queryEntity,
    this.selectEntity,
    super.key,
    this.controller,
    this.decoration,
    this.autovalidateMode,
    this.minInputLength = 1,
    this.autofocus = false,
    this.focusNode,
    this.enabled = true,
    this.noEntries,
  });

  final FSStringIdInputController? controller;
  final InputDecoration? decoration;
  final AutovalidateMode? autovalidateMode;
  final EntityToStringIdFunction<T> entityToId;
  final EntityToLabelFunction<T> entityToLabel;
  final StringIdToEntityFunction<T> fetchEntity;
  final QueryEntityFunction<T> queryEntity;
  final SelectEntityFunction<T>? selectEntity;
  final int minInputLength;
  final bool autofocus;
  final FocusNode? focusNode;
  final bool enabled;

  /// Widget to be displayed if no results are found using the provided input.
  /// Defaults to a `ListTile` with a title containing "No entries found"
  final Widget? noEntries;

  @override
  State<FSStringIdInputField<T>> createState() => _FSStringIdInputFieldState<T>();
}

class _FSStringIdInputFieldState<T> extends State<FSStringIdInputField<T>> {
  static const _itemHeight = 48.0; // default single line list tile height

  late final TextEditingController _textInputController;
  late final FSStringIdInputController _localController;
  late final FocusNode _localFocusNode;
  late final LayerLink _layerLink;
  late final ScrollController _scrollController;

  OverlayEntry? _overlayEntry;
  bool _loading = false;
  List<T> _list = [];
  bool _entitiesLoaded = false;
  T? _selectedEntity;
  CancelableOperation? _queryCancel;
  Timer? _debouncer;
  int? _focusedIndex;

  FocusNode get _actualFocusNode => widget.focusNode ?? _localFocusNode;
  FSStringIdInputController get _actualController => widget.controller ?? _localController;

  @override
  void initState() {
    super.initState();

    _textInputController = TextEditingController();
    _localController = FSStringIdInputController(widget.controller?.value);
    _localFocusNode = FocusNode();
    _layerLink = LayerLink();
    _scrollController = ScrollController();

    if (_actualController.value != null) {
      unawaited(_fetchInitial(_actualController.value!));
    }

    _textInputController.addListener(_onTextChanged);
    _actualController.addListener(_onValueChanged);
    _actualFocusNode.addListener(_updateOverlay);
  }

  Future<void> _fetchInitial(String value) async {
    final entity = await widget.fetchEntity(value);
    _updateSelected(entity);
  }

  @override
  void dispose() {
    _overlayEntry?.remove();
    _overlayEntry?.dispose();

    _actualController.removeListener(_onValueChanged);
    _textInputController.removeListener(_onTextChanged);
    _actualFocusNode.removeListener(_updateOverlay);
    unawaited(_queryCancel?.cancel());
    _debouncer?.cancel();

    _localController.dispose();
    _textInputController.dispose();
    _actualFocusNode.dispose();
    _scrollController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final localizations = FSWidgetLocalizations.of(context)!;

    var actualDecoration = widget.decoration ?? const InputDecoration();
    if (widget.selectEntity != null) {
      actualDecoration = actualDecoration.copyWith(
        suffixIcon: Tooltip(
          message: localizations.idFormFieldSearchEntity,
          child: IconButton(
            onPressed: () async {
              final result = await widget.selectEntity!();
              if (result != null) {
                _select(result);
              }
            },
            icon: const Icon(Icons.search),
          ),
        ),
      );
    }

    return CompositedTransformTarget(
      link: this._layerLink,
      child: CallbackShortcuts(
        bindings: {
          const SingleActivator(LogicalKeyboardKey.enter): _selectFocused,
          const SingleActivator(LogicalKeyboardKey.arrowDown): _focusNext,
          const SingleActivator(LogicalKeyboardKey.arrowUp): _focusPrevious,
        },
        child: TextFormField(
          decoration: actualDecoration,
          controller: _textInputController,
          focusNode: _actualFocusNode,
          autovalidateMode: widget.autovalidateMode,
          autofocus: widget.autofocus,
          validator: (String? valueText) {
            if (valueText != null && valueText.isNotEmpty && _actualController.value == null) {
              return localizations.idFormFieldSelectionRequired;
            }
            return _actualController.validator?.call(valueText);
          },
          enabled: widget.enabled,
          readOnly: !widget.enabled,
        ),
      ),
    );
  }

  void _selectFocused() {
    if (!_loading && _list.isNotEmpty) {
      if (_focusedIndex != null && _focusedIndex! < _list.length) {
        _select(_list[_focusedIndex!]);
      }
    }
  }

  void _focusNext() {
    if (!_loading && _list.isNotEmpty) {
      if (_focusedIndex == null) {
        _focusedIndex = 0;
        _scrollController.jumpTo(0);
        _overlayEntry?.markNeedsBuild();
      } else if (_focusedIndex! < _list.length - 1) {
        _focusedIndex = _focusedIndex! + 1;
        _scrollController.jumpTo(
          min(
            _focusedIndex! * _itemHeight,
            _scrollController.position.maxScrollExtent,
          ),
        );
        _overlayEntry?.markNeedsBuild();
      }
    }
  }

  void _focusPrevious() {
    if (!_loading && _list.isNotEmpty && _focusedIndex != null && _focusedIndex! > 0) {
      _focusedIndex = _focusedIndex! - 1;
      _scrollController.jumpTo(
        min(
          _focusedIndex! * _itemHeight,
          _scrollController.position.maxScrollExtent,
        ),
      );
      _overlayEntry?.markNeedsBuild();
    }
  }

  Future<void> _onValueChanged() async {
    final value = _actualController.value;
    if ((_selectedEntity == null && value == null) ||
        (_selectedEntity != null && widget.entityToId(_selectedEntity as T) == value)) {
      return;
    }

    if (value == null) {
      _updateSelected(null);
    } else {
      final entity = await widget.fetchEntity(value);
      _updateSelected(entity);
    }
  }

  void _onTextChanged([forceInstant = false]) {
    if (_selectedEntity != null && widget.entityToLabel(_selectedEntity as T) == _textInputController.text) {
      return;
    }
    _updateSelected(null, updateText: false);
    if (_debouncer?.isActive ?? false) {
      _debouncer?.cancel();
    }
    if (forceInstant) {
      unawaited(_updateList(_textInputController.text));
    } else {
      _debouncer = Timer(
        const Duration(milliseconds: 300),
        () async => _updateList(_textInputController.text),
      );
    }
  }

  void _select(T entity) {
    _updateSelected(entity);
    _list = [];
    if (mounted) {
      setState(() {
        _entitiesLoaded = false;
      });
    }
    _overlayEntry?.markNeedsBuild();
  }

  void _updateSelected(T? entity, {bool updateText = true}) {
    if (entity == _selectedEntity ||
        (entity != null &&
            _selectedEntity != null &&
            widget.entityToId(entity) == widget.entityToId(_selectedEntity as T))) {
      return;
    }
    _selectedEntity = entity;
    _actualController.value = entity == null ? null : widget.entityToId(entity);
    if (updateText && mounted) {
      if (entity == null) {
        _textInputController.value = const TextEditingValue(
          selection: TextSelection.collapsed(offset: 0),
        );
      } else {
        final text = widget.entityToLabel(entity);
        _textInputController.value = TextEditingValue(
          text: text,
          selection: TextSelection.collapsed(offset: text.length),
        );
      }
    }
  }

  Future<void> _updateOverlay() async {
    if (_actualFocusNode.hasFocus) {
      _onTextChanged(true);
      _overlayEntry?.remove();
      _overlayEntry?.dispose();
      _overlayEntry = _createOverlayEntry();
      Overlay.of(context).insert(_overlayEntry!);
    } else {
      if (_selectedEntity == null && _list.isNotEmpty) {
        final fullMatch = _list.whereType<T?>().firstWhere(
              (element) =>
                  element != null &&
                  widget.entityToLabel(element).toLowerCase() == _textInputController.value.text.toLowerCase(),
              orElse: () => null,
            );
        if (fullMatch != null) {
          _select(fullMatch);
        }
      }
      if (_selectedEntity == null) {
        _selectFocused();
      }

      _overlayEntry?.remove();
      _overlayEntry?.dispose();
      _overlayEntry = null;
    }
  }

  void _setLoading(bool value) {
    if (_loading != value) {
      _loading = value;
    }
  }

  Future<void> _updateList(String? query) async {
    _setLoading(true);
    if (mounted) {
      setState(() {
        _entitiesLoaded = false;
      });
    }
    if (_list.isNotEmpty) {
      _list = [];
      _focusedIndex = null;
    }
    this._overlayEntry?.markNeedsBuild();
    await _queryCancel?.cancel();
    if ((query?.length ?? 0) >= widget.minInputLength) {
      _queryCancel = CancelableOperation.fromFuture(widget.queryEntity(query));

      final tempList = await _queryCancel!.valueOrCancellation();
      if (tempList != null && mounted) {
        _list = tempList;
        _focusedIndex = _list.isNotEmpty ? 0 : null;
        setState(() {
          _entitiesLoaded = true;
        });
        _setLoading(false);
        this._overlayEntry?.markNeedsBuild();
      }
    } else {
      _setLoading(false);
      this._overlayEntry?.markNeedsBuild();
    }
  }

  Widget _listViewBuilder(context) {
    final localizations = FSWidgetLocalizations.of(context)!;
    final colorScheme = Theme.of(context).colorScheme;

    if (_list.isEmpty) {
      if (_entitiesLoaded) {
        return ListView(
          padding: EdgeInsets.zero,
          shrinkWrap: true,
          children: [
            TextFieldTapRegion(
              child: widget.noEntries ??
                  ListTile(
                    title: Text(localizations.idFormFieldNoEntitiesFound),
                  ),
            ),
          ],
        );
      } else {
        return ListView(
          padding: EdgeInsets.zero,
          shrinkWrap: true,
          children: [
            ListTile(
              title: Text(localizations.idFormFieldStartTyping),
            ),
          ],
        );
      }
    }
    return ListView.builder(
      controller: _scrollController,
      itemCount: _list.length,
      itemBuilder: (context, index) {
        final entity = _list[index];
        if (entity == null) {
          return const SizedBox(); // should never be the case
        }
        return TextFieldTapRegion(
          child: InkWell(
            onTap: () async => _select(entity),
            child: ListTile(
              title: Text(widget.entityToLabel(entity)),
              selected: index == _focusedIndex,
              selectedTileColor: colorScheme.secondaryContainer,
              selectedColor: colorScheme.onSecondaryContainer,
            ),
          ),
        );
      },
      padding: EdgeInsets.zero,
      shrinkWrap: true,
    );
  }

  Widget? _listViewContainer(context) => SizedBox(
        height: _calculateHeight(),
        child: _listViewBuilder(context),
      );

  Widget _loadingIndicator() => SizedBox(
        width: 50,
        height: 50,
        child: Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(
              Theme.of(context).colorScheme.secondary,
            ),
          ),
        ),
      );

  double _calculateHeight() {
    if (_list.isEmpty) {
      return _itemHeight;
    } else {
      return min(5.5, _list.length) * _itemHeight;
    }
  }

  OverlayEntry _createOverlayEntry() {
    final renderBox = context.findRenderObject()! as RenderBox;
    final overlaySize = renderBox.size;
    final screenSize = MediaQuery.of(context).size;
    final screenWidth = screenSize.width;

    return OverlayEntry(
      builder: (con) => Positioned(
        width: overlaySize.width,
        child: _selectedEntity != null
            ? const SizedBox()
            : CompositedTransformFollower(
                link: _layerLink,
                showWhenUnlinked: false,
                // 50 is the default height of the input field
                // overlaySize.height is not used since it shifts down with validation errors
                offset: const Offset(0.0, 50.0 + 5.0),
                child: Material(
                  elevation: 4.0,
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: screenWidth,
                      maxWidth: screenWidth,
                      maxHeight: _calculateHeight(),
                    ),
                    child: _loading ? _loadingIndicator() : _listViewContainer(context),
                  ),
                ),
              ),
      ),
    );
  }
}
