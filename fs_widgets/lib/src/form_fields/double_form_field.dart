import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import '../../fs_widgets.dart';

class FSDoubleInputField extends StatefulWidget {
  const FSDoubleInputField({
    super.key,
    this.controller,
    this.decoration,
    this.autovalidateMode,
    this.enabled = true,
    this.onFieldSubmitted,
    this.autofocus = false,
    this.focusNode,
  });

  final FSDoubleInputController? controller;
  final InputDecoration? decoration;
  final AutovalidateMode? autovalidateMode;
  final bool enabled;
  final ValueChanged<String>? onFieldSubmitted;
  final bool autofocus;
  final FocusNode? focusNode;

  @override
  State<FSDoubleInputField> createState() => _FSDoubleInputFieldState();
}

class _FSDoubleInputFieldState extends State<FSDoubleInputField> {
  TextEditingController? _textInputController;
  late final FSDoubleInputController _localController;
  late final FocusNode _localFocusNode;

  FSDoubleInputController get _actualController => widget.controller ?? _localController;
  FocusNode get _actualFocusNode => widget.focusNode ?? _localFocusNode;

  NumberFormat _numberFormat = NumberFormat.decimalPattern();
  NumberFormat _getNumberFormat(BuildContext context) {
    if (_actualController.numberFormat != null) {
      return _actualController.numberFormat!;
    }
    final locale = Localizations.localeOf(context).toString();
    if (_numberFormat.locale != locale) {
      _numberFormat = NumberFormat.decimalPattern(locale);
    }
    return _numberFormat;
  }

  @override
  void initState() {
    super.initState();

    _localController = FSDoubleInputController(widget.controller?.value);
    _localFocusNode = FocusNode();

    _actualController.addListener(_onValueChanged);
    _actualFocusNode.addListener(_onFocusLoss);
  }

  @override
  void didChangeDependencies() {
    if (_textInputController == null) {
      _textInputController = TextEditingController(text: _format(widget.controller?.value));
      _textInputController!.addListener(_onTextChanged);
    }

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _actualController.removeListener(_onValueChanged);
    _textInputController?.removeListener(_onTextChanged);
    _actualFocusNode.removeListener(_onFocusLoss);

    _localController.dispose();
    _textInputController?.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final disabledColor = Theme.of(context).disabledColor;
    final disbledBorder = UnderlineInputBorder(borderSide: BorderSide(color: disabledColor));
    final disabledTextStyle = TextStyle(color: disabledColor);

    var actualDecoration = widget.decoration ?? const InputDecoration();
    if (!widget.enabled) {
      actualDecoration = actualDecoration.copyWith(
        enabledBorder: disbledBorder,
        focusedBorder: disbledBorder,
        labelStyle: disabledTextStyle,
        floatingLabelStyle: disabledTextStyle,
      );
    }

    return TextFormField(
      decoration: actualDecoration,
      controller: _textInputController,
      style: !widget.enabled ? disabledTextStyle : null,
      readOnly: !widget.enabled,
      validator: _actualController.validator == null
          ? null
          : (String? valueText) => _actualController.validator!.call(
                valueText == null ? null : _parse(valueText),
              ),
      keyboardType: const TextInputType.numberWithOptions(
        decimal: true,
        signed: true,
      ),
      inputFormatters: [
        TextInputFormatter.withFunction(
          (
            TextEditingValue oldValue,
            TextEditingValue newValue,
          ) {
            if (newValue.text.trim().isEmpty) {
              return newValue;
            }
            final numberFormat = _getNumberFormat(context);
            if (newValue.text.split(numberFormat.symbols.DECIMAL_SEP).length > 2) {
              return oldValue;
            }
            final groupAfterDecimalSep =
                RegExp('^.*\\${numberFormat.symbols.DECIMAL_SEP}.*\\${numberFormat.symbols.GROUP_SEP}');
            if (groupAfterDecimalSep.hasMatch(newValue.text)) {
              return oldValue;
            }
            final parsedValue = _parse(newValue.text.trim());
            return parsedValue != null ? newValue : oldValue;
          },
        ),
      ],
      autovalidateMode: widget.autovalidateMode,
      onFieldSubmitted: widget.onFieldSubmitted,
      autofocus: widget.autofocus,
      focusNode: _actualFocusNode,
    );
  }

  void _onValueChanged() {
    if (_parse(_textInputController?.text) == _actualController.value) {
      // if the input is a valid format for the actual value the text does not require an update
      return;
    }

    if (_actualController.value == null) {
      _textInputController?.value = TextEditingValue.empty;
    } else {
      final newText = _format(_actualController.value);
      _textInputController?.value = TextEditingValue(
        text: newText,
        selection: TextSelection(
          baseOffset: newText.length,
          extentOffset: newText.length,
        ),
      );
    }
  }

  void _onTextChanged() {
    _actualController.value = _parse(_textInputController?.text);
  }

  void _onFocusLoss() {
    if (_actualFocusNode.hasFocus || _actualController.value == null) {
      return;
    }
    final newText = _format(_actualController.value);

    if (newText != _textInputController?.text) {
      _textInputController?.value = TextEditingValue(
        text: newText,
        selection: TextSelection(
          baseOffset: newText.length,
          extentOffset: newText.length,
        ),
      );
    }
  }

  String _format(double? value) {
    if (value == null) {
      return '';
    } else {
      return _getNumberFormat(context).format(value).trim();
    }
  }

  double? _parse(String? text) {
    final actualText = (text ?? '').trim();
    if (actualText.isEmpty) {
      return null;
    } else {
      return _getNumberFormat(context).parse(actualText) as double;
    }
  }
}
