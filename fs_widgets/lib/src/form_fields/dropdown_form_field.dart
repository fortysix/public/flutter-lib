import 'package:flutter/material.dart';

import '../controller/dropdown_controller.dart';

class FSDropdownInputField<T> extends StatefulWidget {
  const FSDropdownInputField({
    super.key,
    this.controller,
    this.decoration,
    this.autovalidateMode,
    this.enabled = true,
    this.autofocus = false,
  });

  final FSDropdownInputController<T?>? controller;
  final InputDecoration? decoration;
  final AutovalidateMode? autovalidateMode;
  final bool enabled;
  final bool autofocus;

  @override
  State<FSDropdownInputField<T?>> createState() => _FSDropdownInputFieldState();
}

class _FSDropdownInputFieldState<T> extends State<FSDropdownInputField<T?>> {
  late final FSDropdownInputController<T?> _localController;

  FSDropdownInputController<T?> get _actualController =>
      widget.controller ?? _localController;

  @override
  void initState() {
    super.initState();

    _localController =
        FSDropdownInputController<T?>(widget.controller?.value, options: []);
  }

  @override
  void dispose() {
    _localController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) => DropdownButtonFormField<T?>(
        decoration: widget.decoration,
        items: _actualController.options,
        value: _actualController.value,
        validator: _actualController.validator,
        autovalidateMode: widget.autovalidateMode,
        onChanged: widget.enabled
            ? (T? value) {
                _actualController.value = value;
              }
            : null,
        autofocus: widget.autofocus,
      );
}
