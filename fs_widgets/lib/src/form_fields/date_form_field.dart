import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../gen/fs_widget_localizations.dart';
import '../controller/date_controller.dart';
import '../date.dart';

class FSDateInputField extends StatefulWidget {
  const FSDateInputField({
    super.key,
    this.controller,
    this.decoration = const InputDecoration(),
    this.autovalidateMode,
    this.enabled = true,
    this.onFieldSubmitted,
    this.onSaved,
    this.autofocus = false,
    this.focusNode,
  });

  final FSDateInputController? controller;
  final InputDecoration decoration;
  final AutovalidateMode? autovalidateMode;
  final bool enabled;
  final ValueChanged<FSDate?>? onFieldSubmitted;
  final ValueChanged<FSDate?>? onSaved;
  final bool autofocus;
  final FocusNode? focusNode;

  @override
  State<FSDateInputField> createState() => _FSDateInputFieldState();
}

class _FSDateInputFieldState extends State<FSDateInputField> {
  TextEditingController? _textInputController;
  late final FSDateInputController _localController;
  late final FocusNode _localFocusNode;

  FSDateInputController get _actualController => widget.controller ?? _localController;
  FocusNode get _actualFocusNode => widget.focusNode ?? _localFocusNode;

  DateFormat _dateFormat = DateFormat();
  DateFormat _getDateFormat(BuildContext context) {
    final locale = Localizations.localeOf(context).toString();
    if (_dateFormat.locale != locale) {
      _dateFormat = DateFormat.yMd(locale);
    }
    return _dateFormat;
  }

  DateFormat _noYearFormat = DateFormat();
  DateFormat _getNoYearFormat(BuildContext context) {
    final locale = Localizations.localeOf(context).toString();
    if (_noYearFormat.locale != locale) {
      _noYearFormat = DateFormat.Md(locale);
    }
    return _noYearFormat;
  }

  DateFormat _yyyymmddFormat = DateFormat();
  DateFormat _getYYYYMMDDFormat(BuildContext context) {
    final locale = Localizations.localeOf(context).toString();
    if (_yyyymmddFormat.locale != locale) {
      _yyyymmddFormat = DateFormat('yyyy-MM-dd', locale);
    }
    return _yyyymmddFormat;
  }

  @override
  void initState() {
    super.initState();

    _localController = FSDateInputController(widget.controller?.value);
    _localFocusNode = FocusNode();

    _actualController.addListener(_onValueChanged);
    _actualFocusNode.addListener(_onFocusLoss);
  }

  @override
  void didChangeDependencies() {
    if (_textInputController == null) {
      _textInputController = TextEditingController(text: _format(widget.controller?.value));
      _textInputController!.addListener(_onTextChanged);
    }

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _actualController.removeListener(_onValueChanged);
    _textInputController?.removeListener(_onTextChanged);
    _actualFocusNode.removeListener(_onFocusLoss);

    _localController.dispose();
    _textInputController?.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final localizations = FSWidgetLocalizations.of(context)!;
    final theme = Theme.of(context);

    final text = _textInputController?.text ?? '';
    const suffixButtonPadding = EdgeInsets.all(3.0);
    final labelText = widget.decoration.labelText ??
        (widget.decoration.label is Text ? (widget.decoration.label! as Text).data : null);
    final suffixButtons = IntrinsicHeight(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          if (text.isEmpty)
            Tooltip(
              message: localizations.dateFormFieldNow,
              child: InkWell(
                onTap: () => _actualController.value = FSDate.now(),
                child: const Padding(
                  padding: suffixButtonPadding,
                  child: Icon(Icons.today),
                ),
              ),
            )
          else if (text.isNotEmpty)
            Tooltip(
              message: localizations.dateFormFieldClear,
              child: InkWell(
                child: const Padding(
                  padding: suffixButtonPadding,
                  child: Icon(Icons.close),
                ),
                onTap: () => _actualController.value = null,
              ),
            ),
          Tooltip(
            message: localizations.dateFromFieldSelect,
            child: InkWell(
              child: const Padding(
                padding: suffixButtonPadding,
                child: Icon(Icons.edit),
              ),
              onTap: () async => _selectDate(labelText),
            ),
          ),
        ],
      ),
    );
    final actualDecoration = widget.decoration.applyDefaults(theme.inputDecorationTheme).copyWith(
          suffixIcon: widget.enabled ? suffixButtons : null,
        );

    return TextFormField(
      decoration: actualDecoration,
      controller: _textInputController,
      enabled: widget.enabled,
      readOnly: !widget.enabled,
      validator: (String? valueText) {
        if (valueText != null && valueText.isNotEmpty && _actualController.value == null) {
          return localizations.dateFormFieldInvalidFormat;
        }
        return _actualController.validator?.call(_actualController.value);
      },
      keyboardType: TextInputType.datetime,
      autovalidateMode: widget.autovalidateMode,
      onFieldSubmitted: (_) => widget.onFieldSubmitted?.call(_actualController.value),
      autofocus: widget.autofocus,
      onSaved: (_) => widget.onSaved?.call(_actualController.value),
      focusNode: _actualFocusNode,
    );
  }

  void _onValueChanged() {
    if (_parseValue(_textInputController?.text) == _actualController.value) {
      // if the input is a valid format for the actual value the text does not require an update
      return;
    }

    if (_actualController.value == null) {
      _textInputController?.value = TextEditingValue.empty;
    } else {
      final newText = _format(_actualController.value);
      _textInputController?.value = TextEditingValue(
        text: newText,
        selection: TextSelection(
          baseOffset: newText.length,
          extentOffset: newText.length,
        ),
      );
    }
  }

  void _onTextChanged() {
    _actualController.value = _parseValue(_textInputController?.text);
    setState(() {
      // Required for proper updating of suffix icons
    });
  }

  void _onFocusLoss() {
    if (_actualFocusNode.hasFocus || _actualController.value == null) {
      return;
    }
    final newText = _format(_actualController.value);

    if (newText != _textInputController?.text) {
      _textInputController?.value = TextEditingValue(
        text: newText,
        selection: TextSelection(
          baseOffset: newText.length,
          extentOffset: newText.length,
        ),
      );
    }
  }

  String _format(FSDate? date) => date == null ? '' : _getDateFormat(context).format(date.toDateTime());

  FSDate? _parseValue(String? text) {
    final valueText = (text ?? '').trim();
    FSDate? parsed;
    parsed = _parseByFormat(
        valueText, _getDateFormat(context), (date) => FSDate(_adjustYear(date.year), date.month, date.day));
    parsed ??=
        _parseByFormat(valueText, _getNoYearFormat(context), (date) => FSDate(FSDate.now().year, date.month, date.day));
    parsed ??= _parseByFormat(
        valueText, _getYYYYMMDDFormat(context), (date) => FSDate(_adjustYear(date.year), date.month, date.day));
    return parsed;
  }

  FSDate? _parseByFormat(
    String valueText,
    DateFormat format,
    FSDate Function(FSDate date) modifier,
  ) {
    FSDate? parsed;
    try {
      final parsedDateTime = format.parseLoose(valueText);
      parsed = modifier(parsedDateTime.toFSDate());
    } on FormatException catch (_) {
      // the input has the wrong format and cannot be parsed
      // value will be treated as null
      parsed = null;
    } catch (e) {
      rethrow;
    }
    return parsed;
  }

  int _adjustYear(int year) {
    if (year < 70) {
      return year + 2000;
    } else if (year < 100) {
      return year + 1900;
    } else {
      return year;
    }
  }

  Future<FSDate?> _selectDate(String? label) async {
    final start = _actualController.value ?? FSDate.now();
    final first = FSDate(start.year - 100, start.month, start.day);
    final last = FSDate(start.year + 100, start.month, start.day);
    final pickedDate = await showDatePicker(
      context: context,
      initialDate: start.toDateTime(),
      firstDate: first.toDateTime(),
      lastDate: last.toDateTime(),
      helpText: label,
      builder: (context, child) => child ?? const SizedBox(),
    );
    if (pickedDate == null) {
      return null;
    }

    final newDate = FSDate(
      pickedDate.year,
      pickedDate.month,
      pickedDate.day,
    );
    _actualController.value = newDate;
    return newDate;
  }
}
