import 'package:flutter/material.dart';

import '../controller/string_controller.dart';

class FSStringInputField extends StatefulWidget {
  const FSStringInputField({
    super.key,
    this.controller,
    this.decoration,
    this.autovalidateMode,
    this.enabled = true,
    this.minLines,
    this.maxLines = 1,
    this.onFieldSubmitted,
    this.autofocus = false,
    this.textCapitalization = TextCapitalization.none,
    this.keyboardType,
  });

  final FSStringInputController? controller;
  final InputDecoration? decoration;
  final AutovalidateMode? autovalidateMode;
  final bool enabled;
  final int? minLines;
  final int? maxLines;
  final ValueChanged<String>? onFieldSubmitted;
  final bool autofocus;
  final TextCapitalization textCapitalization;
  final TextInputType? keyboardType;

  @override
  State<FSStringInputField> createState() => _FSStringInputFieldState();
}

class _FSStringInputFieldState extends State<FSStringInputField> {
  late final TextEditingController _textInputController;
  late final FSStringInputController _localController;

  FSStringInputController get _actualController => widget.controller ?? _localController;

  @override
  void initState() {
    super.initState();

    _textInputController = TextEditingController(text: widget.controller?.value);
    _localController = FSStringInputController(_textInputController.text);

    _textInputController.addListener(_onTextChanged);
    _actualController.addListener(_onValueChanged);
  }

  @override
  void dispose() {
    _actualController.removeListener(_onValueChanged);
    _textInputController.removeListener(_onTextChanged);

    _localController.dispose();
    _textInputController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final disabledColor = Theme.of(context).disabledColor;
    final disbledBorder = UnderlineInputBorder(borderSide: BorderSide(color: disabledColor));
    final disabledTextStyle = TextStyle(color: disabledColor);

    var actualDecoration = widget.decoration ?? const InputDecoration();
    if (!widget.enabled) {
      actualDecoration = actualDecoration.copyWith(
        enabledBorder: disbledBorder,
        focusedBorder: disbledBorder,
        labelStyle: disabledTextStyle,
        floatingLabelStyle: disabledTextStyle,
      );
    }

    return TextFormField(
      decoration: actualDecoration,
      controller: _textInputController,
      style: !widget.enabled ? disabledTextStyle : null,
      readOnly: !widget.enabled,
      validator: _actualController.validator,
      minLines: widget.minLines,
      maxLines: widget.maxLines,
      autovalidateMode: widget.autovalidateMode,
      onFieldSubmitted: widget.onFieldSubmitted,
      autofocus: widget.autofocus,
      textCapitalization: widget.textCapitalization,
      keyboardType: widget.keyboardType,
    );
  }

  void _onValueChanged() {
    _textInputController.value = _textInputController.value.copyWith(text: _actualController.value);
  }

  void _onTextChanged() {
    _actualController.value = _textInputController.text;
  }
}
