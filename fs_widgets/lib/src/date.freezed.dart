// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'date.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

FSDate _$FSDateFromJson(Map<String, dynamic> json) {
  return _FSDate.fromJson(json);
}

/// @nodoc
mixin _$FSDate {
  int get year => throw _privateConstructorUsedError;
  int get month => throw _privateConstructorUsedError;
  int get day => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FSDateCopyWith<FSDate> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FSDateCopyWith<$Res> {
  factory $FSDateCopyWith(FSDate value, $Res Function(FSDate) then) =
      _$FSDateCopyWithImpl<$Res, FSDate>;
  @useResult
  $Res call({int year, int month, int day});
}

/// @nodoc
class _$FSDateCopyWithImpl<$Res, $Val extends FSDate>
    implements $FSDateCopyWith<$Res> {
  _$FSDateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? year = null,
    Object? month = null,
    Object? day = null,
  }) {
    return _then(_value.copyWith(
      year: null == year
          ? _value.year
          : year // ignore: cast_nullable_to_non_nullable
              as int,
      month: null == month
          ? _value.month
          : month // ignore: cast_nullable_to_non_nullable
              as int,
      day: null == day
          ? _value.day
          : day // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$FSDateImplCopyWith<$Res> implements $FSDateCopyWith<$Res> {
  factory _$$FSDateImplCopyWith(
          _$FSDateImpl value, $Res Function(_$FSDateImpl) then) =
      __$$FSDateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int year, int month, int day});
}

/// @nodoc
class __$$FSDateImplCopyWithImpl<$Res>
    extends _$FSDateCopyWithImpl<$Res, _$FSDateImpl>
    implements _$$FSDateImplCopyWith<$Res> {
  __$$FSDateImplCopyWithImpl(
      _$FSDateImpl _value, $Res Function(_$FSDateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? year = null,
    Object? month = null,
    Object? day = null,
  }) {
    return _then(_$FSDateImpl(
      null == year
          ? _value.year
          : year // ignore: cast_nullable_to_non_nullable
              as int,
      null == month
          ? _value.month
          : month // ignore: cast_nullable_to_non_nullable
              as int,
      null == day
          ? _value.day
          : day // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$FSDateImpl extends _FSDate {
  const _$FSDateImpl(this.year, this.month, this.day) : super._();

  factory _$FSDateImpl.fromJson(Map<String, dynamic> json) =>
      _$$FSDateImplFromJson(json);

  @override
  final int year;
  @override
  final int month;
  @override
  final int day;

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FSDateImpl &&
            (identical(other.year, year) || other.year == year) &&
            (identical(other.month, month) || other.month == month) &&
            (identical(other.day, day) || other.day == day));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, year, month, day);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FSDateImplCopyWith<_$FSDateImpl> get copyWith =>
      __$$FSDateImplCopyWithImpl<_$FSDateImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$FSDateImplToJson(
      this,
    );
  }
}

abstract class _FSDate extends FSDate {
  const factory _FSDate(final int year, final int month, final int day) =
      _$FSDateImpl;
  const _FSDate._() : super._();

  factory _FSDate.fromJson(Map<String, dynamic> json) = _$FSDateImpl.fromJson;

  @override
  int get year;
  @override
  int get month;
  @override
  int get day;
  @override
  @JsonKey(ignore: true)
  _$$FSDateImplCopyWith<_$FSDateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
