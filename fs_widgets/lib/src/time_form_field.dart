import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../gen/fs_widget_localizations.dart';

typedef TimeOfDayCallback = void Function(TimeOfDay? time);
typedef _TimeOfDayModifier = TimeOfDay Function(TimeOfDay time);

extension TimoOfDayToDateTimeExtension on TimeOfDay {
  DateTime toDateTime() {
    final now = DateTime.now();
    return DateTime(now.year, now.month, now.day, hour, minute);
  }
}

class FSTimeFormField extends FormField<TimeOfDay?> {
  FSTimeFormField({
    super.key,
    super.initialValue,
    this.decoration = const InputDecoration(),
    super.onSaved,
    super.validator,
    super.autovalidateMode,
    super.enabled,
    super.restorationId,
    TextInputAction? textInputAction,
    this.onChanged,
    this.pickerTheme,
    this.focusNode,
    this.onNowSet,
  }) : super(builder: (state) {
          state = state as FSTimeFormFieldState;
          final theme = Theme.of(state.context);
          final localizations = FSWidgetLocalizations.of(state.context)!;

          const suffixButtonPadding = EdgeInsets.all(3.0);
          final suffixButtons = IntrinsicHeight(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                if (state.controller.text.isEmpty)
                  Tooltip(
                    message: localizations.timeFormFieldNow,
                    child: InkWell(
                      focusNode: state._buttonsFocusNode,
                      onTap: state.setNow,
                      child: const Padding(
                        padding: suffixButtonPadding,
                        child: Icon(Icons.access_time),
                      ),
                    ),
                  )
                else if (state.controller.text.isNotEmpty)
                  Tooltip(
                    message: localizations.timeFormFieldClear,
                    child: InkWell(
                      focusNode: state._buttonsFocusNode,
                      child: const Padding(
                        padding: suffixButtonPadding,
                        child: Icon(Icons.close),
                      ),
                      onTap: () =>
                          (state as FSTimeFormFieldState).setValue(null),
                    ),
                  ),
                Tooltip(
                  message: localizations.timeFromFieldSelect,
                  child: InkWell(
                    focusNode: state._buttonsFocusNode,
                    child: const Padding(
                      padding: suffixButtonPadding,
                      child: Icon(Icons.edit),
                    ),
                    onTap: () => (state as FSTimeFormFieldState)
                        .selectTime(state.labelText),
                  ),
                ),
              ],
            ),
          );
          final effectiveDecoration =
              decoration.applyDefaults(theme.inputDecorationTheme).copyWith(
                    suffixIcon: enabled ? suffixButtons : null,
                    errorText: state.formatError
                        ? FSWidgetLocalizations.of(state.context)!
                            .timeFormFieldInvalidFormat
                        : state.errorText,
                  );

          return TextField(
            controller: state.controller,
            focusNode: state._actualFocusNode,
            decoration: effectiveDecoration,
            keyboardType: TextInputType.datetime,
            enabled: enabled,
            textInputAction: textInputAction,
            onSubmitted: (value) {
              if ((state as FSTimeFormFieldState).isValid) {
                state.setValue(state.parseValue(value));
              }
            },
            onChanged: (value) => state
                .didChange((state as FSTimeFormFieldState).parseValue(value)),
          );
        });

  final TimeOfDayCallback? onChanged;
  final InputDecoration decoration;
  final ThemeData? pickerTheme;
  final FocusNode? focusNode;
  final VoidCallback? onNowSet;

  @override
  FSTimeFormFieldState createState() => FSTimeFormFieldState();
}

class FSTimeFormFieldState extends FormFieldState<TimeOfDay?> {
  TextEditingController? _controller;
  TextEditingController get controller {
    _controller ??= TextEditingController(
      text: widget.initialValue == null
          ? ''
          : getTimeFormat(context).format(widget.initialValue!.toDateTime()),
    );
    return _controller!;
  }

  final _inputFieldFocusNode = FocusNode();
  FocusNode get _actualFocusNode => widget.focusNode ?? _inputFieldFocusNode;

  final _buttonsFocusNode =
      FocusNode(canRequestFocus: false, skipTraversal: true);
  FocusNode get buttonsFocusNode => _buttonsFocusNode;

  DateFormat _dateFormat = DateFormat();
  DateFormat getTimeFormat(BuildContext context) {
    final locale = Localizations.localeOf(context).toString();
    if (_dateFormat.locale != locale) {
      _dateFormat = DateFormat.jm(locale);
    }
    return _dateFormat;
  }

  DateFormat _noMinFormat = DateFormat();
  DateFormat getNoMinFormat(BuildContext context) {
    final locale = Localizations.localeOf(context).toString();
    if (_noMinFormat.locale != locale) {
      _noMinFormat = DateFormat.j(locale);
    }
    return _noMinFormat;
  }

  bool _formatError = false;
  bool get formatError => _formatError;

  String? get labelText =>
      widget.decoration.labelText ??
      (widget.decoration.label is Text
          ? (widget.decoration.label as Text).data
          : null);

  @override
  FSTimeFormField get widget => super.widget as FSTimeFormField;

  @override
  void initState() {
    super.initState();

    _actualFocusNode.addListener(_onFocusLoss);
  }

  @override
  void didChange(TimeOfDay? value) {
    super.didChange(value);

    if (widget.onChanged != null) {
      widget.onChanged!(value);
    }
  }

  @override
  void dispose() {
    _actualFocusNode.removeListener(_onFocusLoss);

    _controller?.dispose();
    _actualFocusNode.dispose();
    _buttonsFocusNode.dispose();

    super.dispose();
  }

  @override
  void setValue(TimeOfDay? value) {
    if (value == null) {
      controller.value = TextEditingValue.empty;
    } else {
      final newText = getTimeFormat(context).format(value.toDateTime());
      controller.value = TextEditingValue(
        text: newText,
        selection: TextSelection(
          baseOffset: newText.length,
          extentOffset: newText.length,
        ),
      );
    }

    if (value != this.value) {
      super.setValue(value);

      didChange(value);
    }
  }

  void updateValue(TimeOfDay? value) {
    setValue(value);
  }

  @override
  bool validate() {
    if (value == null && controller.text.isNotEmpty) {
      _formatError = true;
      return false;
    } else {
      _formatError = false;
      return super.validate();
    }
  }

  @override
  bool get isValid {
    return super.isValid &&
        ((value == null && controller.text.isEmpty) ||
            (value != null &&
                parseValue(controller.text) != null &&
                getTimeFormat(context)
                        .format(parseValue(controller.text)!.toDateTime()) ==
                    getTimeFormat(context).format(value!.toDateTime())));
  }

  void _onFocusLoss() {
    if (!_actualFocusNode.hasFocus && isValid) {
      setValue(value);
    }
  }

  Future<TimeOfDay?> selectTime(String? label) async {
    final TimeOfDay? pickedTime = await showTimePicker(
      context: context,
      initialTime: value ?? const TimeOfDay(hour: 0, minute: 0),
      helpText: label,
      builder: (context, child) {
        return Theme(
          data: widget.pickerTheme ?? Theme.of(context),
          child: child ?? const SizedBox(),
        );
      },
    );
    if (pickedTime == null) {
      return null;
    }

    setValue(pickedTime);
    return pickedTime;
  }

  void setNow() {
    setValue(TimeOfDay.now());
    if (widget.onNowSet != null) {
      widget.onNowSet!();
    }
  }

  TimeOfDay? parseValue(String? valueText) {
    valueText ??= '';
    final splitValue = valueText.split(':');
    if (splitValue.length == 2 && splitValue[1].length == 1) {
      // prevent single digit minute input
      return null;
    }

    TimeOfDay? parsed;
    parsed = _parseValue(valueText, getTimeFormat(context));
    parsed ??= _parseValue(valueText, getNoMinFormat(context), (time) {
      return TimeOfDay(hour: time.hour, minute: 0);
    });
    return parsed;
  }

  TimeOfDay? _parseValue(String valueText, DateFormat format,
      [_TimeOfDayModifier? modifier]) {
    TimeOfDay? parsed;
    try {
      final parsedDateTime = format.parseLoose(valueText);
      if (modifier != null) {
        parsed = modifier(TimeOfDay.fromDateTime(parsedDateTime));
      } else {
        parsed = TimeOfDay.fromDateTime(parsedDateTime);
      }
    } on FormatException catch (_) {
      // the input has the wrong format and cannot be parsed
      // value will be treated as null
      parsed = null;
    } catch (e) {
      rethrow;
    }
    return parsed;
  }
}
