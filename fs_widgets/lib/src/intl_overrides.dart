import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_custom.dart';

import 'intl_overrides_de.dart';

abstract class FSIntlOverrides {
  static const LocalizationsDelegate<void> delegate =
      _FSIntlOverridesDelegate();
}

class _FSIntlOverridesDelegate extends LocalizationsDelegate<void> {
  const _FSIntlOverridesDelegate();

  @override
  Future<void> load(Locale locale) async {
    switch (locale.languageCode) {
      case 'de':
        initializeDateFormattingCustom(
          locale: deLocale.toString(),
          symbols: deSymbols,
          patterns: dePatterns,
        );
    }
  }

  @override
  bool isSupported(Locale locale) => true;

  @override
  bool shouldReload(_FSIntlOverridesDelegate old) => false;
}
