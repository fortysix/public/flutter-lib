// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'date.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$FSDateImpl _$$FSDateImplFromJson(Map<String, dynamic> json) => _$FSDateImpl(
      (json['year'] as num).toInt(),
      (json['month'] as num).toInt(),
      (json['day'] as num).toInt(),
    );

Map<String, dynamic> _$$FSDateImplToJson(_$FSDateImpl instance) =>
    <String, dynamic>{
      'year': instance.year,
      'month': instance.month,
      'day': instance.day,
    };
