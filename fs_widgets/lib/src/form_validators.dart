import 'package:flutter/material.dart';

import '../gen/fs_widget_localizations.dart';

class FSFormFieldValidators {
  static FormFieldValidator<T> compose<T>(
      List<FormFieldValidator<T>> validators) {
    return (value) {
      for (final validator in validators) {
        final validatorResult = validator.call(value);
        if (validatorResult != null) {
          return validatorResult;
        }
      }
      return null;
    };
  }

  static FormFieldValidator<T> required<T>(BuildContext context,
      {String? errorText}) {
    return (T? value) {
      if (value == null ||
          (value is String && value.trim().isEmpty) ||
          (value is Iterable && value.isEmpty) ||
          (value is Map && value.isEmpty)) {
        return errorText ??
            FSWidgetLocalizations.of(context)!.formFiledRequired;
      }
      return null;
    };
  }
}
