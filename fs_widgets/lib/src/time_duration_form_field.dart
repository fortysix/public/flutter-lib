import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../gen/fs_widget_localizations.dart';

typedef DurationCallback = void Function(Duration? time);

extension _DurationToTimeDurationStringExtension on Duration {
  String toTimeDurationString() {
    final minutes = inMinutes % 60;
    return '$inHours:${minutes < 10 ? '0$minutes' : minutes}';
  }
}

extension _StringToTimeDuration on String {
  Duration toTimeDuration(BuildContext context) {
    if (length == 0) {
      return const Duration();
    }

    final numFormat =
        NumberFormat(null, Localizations.localeOf(context).toString());
    double? parsedNumber;
    try {
      if (!contains(numFormat.symbols.GROUP_SEP)) {
        // prevent "1.5" input (in german locale where decimal divider is ,)
        // from resulting in 15:00 which is a strange user experience
        parsedNumber = numFormat.parse(this).toDouble();
      }
    } on FormatException catch (_) {
    } catch (e) {
      rethrow;
    }
    if (parsedNumber != null && parsedNumber > 0) {
      final minutes = (parsedNumber % 1 * 60).round();
      return Duration(hours: parsedNumber.truncate(), minutes: minutes);
    }

    final parts = split(':');
    if (parts.length > 2) {
      throw const FormatException('Invalid TimeDuration format');
    }

    int? hours;
    if (parts[0].isNotEmpty) {
      hours = int.tryParse(parts[0]);
      if (hours == null || hours < 0) {
        throw const FormatException('Invalid TimeDuration format');
      }
    }

    int? minutes;
    if (parts.length > 1 && parts[1].isNotEmpty) {
      if (parts[1].length == 1) {
        throw const FormatException('Invalid TimeDuration format');
      }
      minutes = int.tryParse(parts[1]);
      if (minutes == null || minutes < 0 || minutes > 59) {
        throw const FormatException('Invalid TimeDuration format');
      }
    }

    return Duration(hours: hours ?? 0, minutes: minutes ?? 0);
  }
}

class FSTimeDurationFormField extends FormField<Duration?> {
  FSTimeDurationFormField({
    super.key,
    super.initialValue,
    this.decoration = const InputDecoration(),
    super.onSaved,
    super.validator,
    super.autovalidateMode,
    super.enabled,
    super.restorationId,
    TextInputAction? textInputAction,
    this.onChanged,
    this.focusNode,
  }) : super(builder: (state) {
          state = state as FSTimeDurationFormFieldState;
          final theme = Theme.of(state.context);
          final localizations = FSWidgetLocalizations.of(state.context)!;

          const suffixButtonPadding = EdgeInsets.all(3.0);
          final suffixButtons = IntrinsicHeight(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                if (state.controller.text.isNotEmpty)
                  Tooltip(
                    message: localizations.timeDurationFormFieldClear,
                    child: InkWell(
                      focusNode: state._buttonsFocusNode,
                      child: const Padding(
                        padding: suffixButtonPadding,
                        child: Icon(Icons.close),
                      ),
                      onTap: () => (state as FSTimeDurationFormFieldState)
                          .setValue(null),
                    ),
                  ),
              ],
            ),
          );
          final effectiveDecoration =
              decoration.applyDefaults(theme.inputDecorationTheme).copyWith(
                    suffixIcon: enabled ? suffixButtons : null,
                    errorText: state.formatError
                        ? FSWidgetLocalizations.of(state.context)!
                            .timeFormFieldInvalidFormat
                        : state.errorText,
                  );

          return TextField(
            controller: state.controller,
            focusNode: state._actualFocusNode,
            decoration: effectiveDecoration,
            keyboardType: TextInputType.datetime,
            enabled: enabled,
            textInputAction: textInputAction,
            onSubmitted: (value) {
              if ((state as FSTimeDurationFormFieldState).isValid) {
                state.setValue(state.parseValue(value));
              }
            },
            onChanged: (value) => state.didChange(
                (state as FSTimeDurationFormFieldState).parseValue(value)),
          );
        });

  final DurationCallback? onChanged;
  final InputDecoration decoration;
  final FocusNode? focusNode;

  @override
  FSTimeDurationFormFieldState createState() => FSTimeDurationFormFieldState();
}

class FSTimeDurationFormFieldState extends FormFieldState<Duration?> {
  TextEditingController? _controller;
  TextEditingController get controller {
    _controller ??= TextEditingController(
      text: widget.initialValue == null
          ? ''
          : widget.initialValue!.toTimeDurationString(),
    );
    return _controller!;
  }

  final _inputFieldFocusNode = FocusNode();
  FocusNode get _actualFocusNode => widget.focusNode ?? _inputFieldFocusNode;

  final _buttonsFocusNode =
      FocusNode(canRequestFocus: false, skipTraversal: true);
  FocusNode get buttonsFocusNode => _buttonsFocusNode;

  bool _formatError = false;
  bool get formatError => _formatError;

  String? get labelText =>
      widget.decoration.labelText ??
      (widget.decoration.label is Text
          ? (widget.decoration.label as Text).data
          : null);

  @override
  FSTimeDurationFormField get widget => super.widget as FSTimeDurationFormField;

  @override
  void initState() {
    super.initState();

    _actualFocusNode.addListener(_onFocusLoss);
  }

  @override
  void didChange(Duration? value) {
    super.didChange(value);

    if (widget.onChanged != null) {
      widget.onChanged!(value);
    }
  }

  @override
  void dispose() {
    _actualFocusNode.removeListener(_onFocusLoss);

    _controller?.dispose();
    _actualFocusNode.dispose();
    _buttonsFocusNode.dispose();

    super.dispose();
  }

  @override
  void setValue(Duration? value) {
    if (value == null) {
      controller.value = TextEditingValue.empty;
    } else {
      final newText = value.toTimeDurationString();
      controller.value = TextEditingValue(
        text: newText,
        selection: TextSelection(
          baseOffset: newText.length,
          extentOffset: newText.length,
        ),
      );
    }

    if (value != this.value) {
      super.setValue(value);

      didChange(value);
    }
  }

  void updateValue(Duration? value) {
    setValue(value);
  }

  @override
  bool validate() {
    if (value == null && controller.text.isNotEmpty) {
      _formatError = true;
      return false;
    } else {
      _formatError = false;
      return super.validate();
    }
  }

  @override
  bool get isValid {
    return super.isValid &&
        ((value == null && controller.text.isEmpty) ||
            (value != null &&
                parseValue(controller.text) != null &&
                parseValue(controller.text)!.toTimeDurationString() ==
                    value!.toTimeDurationString()));
  }

  void _onFocusLoss() {
    if (!_actualFocusNode.hasFocus && isValid) {
      setValue(value);
    }
  }

  Duration? parseValue(String? valueText) {
    valueText ??= '';
    Duration? parsed;

    try {
      parsed = valueText.toTimeDuration(context);
    } on FormatException catch (_) {
      // the input has the wrong format and cannot be parsed
      // value will be treated as null
      parsed = null;
    } catch (e) {
      rethrow;
    }
    return parsed;
  }
}
