import 'package:freezed_annotation/freezed_annotation.dart';

part 'date.freezed.dart';
part 'date.g.dart';

@Freezed(toStringOverride: false)
class FSDate with _$FSDate {
  const factory FSDate(
    int year,
    int month,
    int day,
  ) = _FSDate;

  const FSDate._();

  factory FSDate.fromJson(Map<String, Object?> json) => _$FSDateFromJson(json);

  factory FSDate.fromJsonStr(String json) {
    final split = json.substring(0, 10).split('-');
    return FSDate(int.parse(split[0]), int.parse(split[1]), int.parse(split[2]));
  }

  String toJsonStr() {
    final yyyy = year.toString().padLeft(4, '0');
    final mm = month.toString().padLeft(2, '0');
    final dd = day.toString().padLeft(2, '0');

    return '$yyyy-$mm-$dd';
  }

  /// The current date
  static FSDate now({bool utc = false}) {
    var now = DateTime.now();
    if (utc) {
      now = now.toUtc();
    }
    return now.toFSDate();
  }

  @override
  String toString() => 'FSDate(${toJsonStr()})';
}

extension FSDateExtension on FSDate {
  /// Convert to a [DateTime].
  DateTime toDateTime({
    bool utc = false,
    int hour = 0,
    int minute = 0,
  }) {
    if (utc) {
      return DateTime.utc(year, month, day, hour, minute);
    } else {
      return DateTime(year, month, day, hour, minute);
    }
  }
}

extension FSDateTimeToDate on DateTime {
  FSDate toFSDate() => FSDate(year, month, day);
}
