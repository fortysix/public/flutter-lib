import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import './date.dart';
import '../gen/fs_widget_localizations.dart';

typedef FSDateCallback = void Function(FSDate? date);
typedef _FSDateModifier = FSDate Function(FSDate date);

@Deprecated('Use FSDateInputField instead')
class FSDateFormField extends FormField<FSDate?> {
  @Deprecated('Use FSDateInputField instead')
  FSDateFormField({
    super.key,
    super.initialValue,
    InputDecoration decoration = const InputDecoration(),
    super.onSaved,
    super.validator,
    super.autovalidateMode,
    super.enabled,
    super.restorationId,
    TextInputAction? textInputAction,
    this.onChanged,
    this.onTodaySet,
    VoidCallback? onDateSelected,
    this.pickerTheme,
    this.focusNode,
  }) : super(builder: (state) {
          state = state as FSDateFormFieldState;
          final theme = Theme.of(state.context);
          final localizations = FSWidgetLocalizations.of(state.context)!;

          final labelText = decoration.labelText ?? (decoration.label is Text ? (decoration.label as Text).data : null);
          const suffixButtonPadding = EdgeInsets.all(3.0);
          final suffixButtons = IntrinsicHeight(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                if (state.controller.text.isEmpty)
                  Tooltip(
                    message: localizations.dateFormFieldNow,
                    child: InkWell(
                      focusNode: state._buttonsFocusNode,
                      onTap: state.setToday,
                      child: const Padding(
                        padding: suffixButtonPadding,
                        child: Icon(Icons.today),
                      ),
                    ),
                  )
                else if (state.controller.text.isNotEmpty)
                  Tooltip(
                    message: localizations.dateFormFieldClear,
                    child: InkWell(
                      focusNode: state._buttonsFocusNode,
                      child: const Padding(
                        padding: suffixButtonPadding,
                        child: Icon(Icons.close),
                      ),
                      onTap: () => (state as FSDateFormFieldState).setValue(null),
                    ),
                  ),
                Tooltip(
                  message: localizations.dateFromFieldSelect,
                  child: InkWell(
                    focusNode: state._buttonsFocusNode,
                    child: const Padding(
                      padding: suffixButtonPadding,
                      child: Icon(Icons.edit),
                    ),
                    onTap: () async {
                      final date = await (state as FSDateFormFieldState).selectDate(labelText);
                      if (date != null && onDateSelected != null) {
                        onDateSelected();
                      }
                    },
                  ),
                ),
              ],
            ),
          );
          final effectiveDecoration = decoration.applyDefaults(theme.inputDecorationTheme).copyWith(
                suffixIcon: enabled ? suffixButtons : null,
                errorText: state.formatError
                    ? FSWidgetLocalizations.of(state.context)!.dateFormFieldInvalidFormat
                    : state.errorText,
              );

          return TextField(
            controller: state.controller,
            focusNode: state._actualFocusNode,
            decoration: effectiveDecoration,
            keyboardType: TextInputType.datetime,
            enabled: enabled,
            textInputAction: textInputAction,
            onSubmitted: (value) {
              if ((state as FSDateFormFieldState).isValid) {
                state.setValue(state.parseValue(value));
              }
            },
            onChanged: (value) => state.didChange((state as FSDateFormFieldState).parseValue(value)),
          );
        });

  final FSDateCallback? onChanged;
  final ThemeData? pickerTheme;
  final FocusNode? focusNode;
  final VoidCallback? onTodaySet;

  @override
  FSDateFormFieldState createState() => FSDateFormFieldState();
}

class FSDateFormFieldState extends FormFieldState<FSDate?> {
  TextEditingController? _controller;
  TextEditingController get controller {
    _controller ??= TextEditingController(
      text: widget.initialValue == null ? '' : getDateFormat(context).format(widget.initialValue!.toDateTime()),
    );
    return _controller!;
  }

  final _inputFieldFocusNode = FocusNode();
  FocusNode get _actualFocusNode => widget.focusNode ?? _inputFieldFocusNode;

  final _buttonsFocusNode = FocusNode(canRequestFocus: false, skipTraversal: true);
  FocusNode get buttonsFocusNode => _buttonsFocusNode;

  DateFormat _dateFormat = DateFormat();
  DateFormat getDateFormat(BuildContext context) {
    final locale = Localizations.localeOf(context).toString();
    if (_dateFormat.locale != locale) {
      _dateFormat = DateFormat.yMd(locale);
    }
    return _dateFormat;
  }

  DateFormat _noYearFormat = DateFormat();
  DateFormat getNoYearFormat(BuildContext context) {
    final locale = Localizations.localeOf(context).toString();
    if (_noYearFormat.locale != locale) {
      _noYearFormat = DateFormat.Md(locale);
    }
    return _noYearFormat;
  }

  DateFormat _yyyymmddFormat = DateFormat();
  DateFormat getYYYYMMDDFormat(BuildContext context) {
    final locale = Localizations.localeOf(context).toString();
    if (_yyyymmddFormat.locale != locale) {
      _yyyymmddFormat = DateFormat('yyyy-MM-dd', locale);
    }
    return _yyyymmddFormat;
  }

  bool _formatError = false;
  bool get formatError => _formatError;

  @override
  FSDateFormField get widget => super.widget as FSDateFormField;

  @override
  void initState() {
    super.initState();

    _actualFocusNode.addListener(_onFocusLoss);
  }

  @override
  void didChange(FSDate? value) {
    super.didChange(value);

    if (widget.onChanged != null) {
      widget.onChanged!(value);
    }
  }

  @override
  void dispose() {
    _actualFocusNode.removeListener(_onFocusLoss);

    _controller?.dispose();
    _actualFocusNode.dispose();
    _buttonsFocusNode.dispose();

    super.dispose();
  }

  @override
  void setValue(FSDate? value) {
    if (value == null) {
      controller.value = TextEditingValue.empty;
    } else {
      final newText = getDateFormat(context).format(value.toDateTime());
      controller.value = TextEditingValue(
        text: newText,
        selection: TextSelection(
          baseOffset: newText.length,
          extentOffset: newText.length,
        ),
      );
    }

    if (value != this.value) {
      super.setValue(value);

      didChange(value);
    }
  }

  void updateValue(FSDate? value) {
    setValue(value);
  }

  @override
  bool validate() {
    if (value == null && controller.text.isNotEmpty) {
      _formatError = true;
      return false;
    } else {
      _formatError = false;
      return super.validate();
    }
  }

  @override
  bool get isValid {
    return super.isValid &&
        ((value == null && controller.text.isEmpty) ||
            (value != null &&
                parseValue(controller.text) != null &&
                getDateFormat(context).format(parseValue(controller.text)!.toDateTime()) ==
                    getDateFormat(context).format(value!.toDateTime())));
  }

  void _onFocusLoss() {
    if (!_actualFocusNode.hasFocus && isValid) {
      setValue(value);
    }
  }

  Future<FSDate?> selectDate(String? label) async {
    final start = value ?? FSDate.now();
    final first = FSDate(start.year - 100, start.month, start.day);
    final last = FSDate(start.year + 100, start.month, start.day);
    final DateTime? pickedDate = await showDatePicker(
      context: context,
      initialDate: start.toDateTime(),
      firstDate: first.toDateTime(),
      lastDate: last.toDateTime(),
      helpText: label,
      builder: (context, child) {
        return Theme(
          data: widget.pickerTheme ?? Theme.of(context),
          child: child ?? const SizedBox(),
        );
      },
    );
    if (pickedDate == null) {
      return null;
    }

    final newDate = FSDate(
      pickedDate.year,
      pickedDate.month,
      pickedDate.day,
    );
    setValue(newDate);
    return newDate;
  }

  void setToday() {
    setValue(FSDate.now());
    if (widget.onTodaySet != null) {
      widget.onTodaySet!();
    }
  }

  FSDate? parseValue(String? valueText) {
    valueText ??= '';
    FSDate? parsed;
    parsed = _parseValue(valueText, getDateFormat(context), (date) {
      return FSDate(_adjustYear(date.year), date.month, date.day);
    });
    parsed ??= _parseValue(valueText, getNoYearFormat(context), (date) {
      return FSDate(FSDate.now().year, date.month, date.day);
    });
    parsed ??= _parseValue(valueText, getYYYYMMDDFormat(context), (date) {
      return FSDate(_adjustYear(date.year), date.month, date.day);
    });
    return parsed;
  }

  FSDate? _parseValue(
    String valueText,
    DateFormat format,
    _FSDateModifier modifier,
  ) {
    FSDate? parsed;
    try {
      final parsedDateTime = format.parseLoose(valueText);
      parsed = modifier(parsedDateTime.toFSDate());
    } on FormatException catch (_) {
      // the input has the wrong format and cannot be parsed
      // value will be treated as null
      parsed = null;
    } catch (e) {
      rethrow;
    }
    return parsed;
  }

  int _adjustYear(int year) {
    if (year < 70) {
      return year + 2000;
    } else if (year < 100) {
      return year + 1900;
    } else {
      return year;
    }
  }
}
