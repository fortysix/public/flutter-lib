import '../date.dart';
import 'generic_controller.dart';

class FSDateInputController extends FSInputController<FSDate?> {
  FSDateInputController(FSDate? value, {super.validator}) : super(value: value);
}
