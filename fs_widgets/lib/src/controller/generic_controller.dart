import 'package:flutter/material.dart';

class FSInputController<T> extends ValueNotifier<T> {
  FSInputController({required T value, this.validator}) : super(value);

  final FormFieldValidator<T>? validator;
}
