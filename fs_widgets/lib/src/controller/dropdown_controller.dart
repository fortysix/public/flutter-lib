import 'package:flutter/material.dart';

import 'generic_controller.dart';

class FSDropdownInputController<T> extends FSInputController<T?> {
  FSDropdownInputController(
    T? value, {
    super.validator,
    required this.options,
  }) : super(value: value);

  final List<DropdownMenuItem<T?>> options;
}
