import 'generic_controller.dart';

class FSBoolInputController extends FSInputController<bool?> {
  FSBoolInputController(bool? value, {super.validator}) : super(value: value);
}
