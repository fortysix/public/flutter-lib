import 'generic_controller.dart';

class FSStringIdInputController extends FSInputController<String?> {
  FSStringIdInputController(String? value, {super.validator})
      : super(value: value);
}
