import 'package:intl/intl.dart';

import 'generic_controller.dart';

class FSIntInputController extends FSInputController<int?> {
  FSIntInputController(
    int? value, {
    super.validator,
    this.numberFormat,
  }) : super(value: value);

  final NumberFormat? numberFormat;
}
