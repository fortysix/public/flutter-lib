import 'generic_controller.dart';

class FSEuroInputController extends FSInputController<double?> {
  FSEuroInputController(
    double? value, {
    super.validator,
  }) : super(value: value);
}
