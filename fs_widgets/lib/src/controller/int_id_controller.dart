import 'generic_controller.dart';

class FSIntIdInputController extends FSInputController<int?> {
  FSIntIdInputController(int? value, {super.validator}) : super(value: value);
}
