import 'package:intl/intl.dart';

import 'generic_controller.dart';

class FSDoubleInputController extends FSInputController<double?> {
  FSDoubleInputController(
    double? value, {
    super.validator,
    this.numberFormat,
  }) : super(value: value);

  final NumberFormat? numberFormat;
}
