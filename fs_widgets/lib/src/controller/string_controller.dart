import 'generic_controller.dart';

class FSStringInputController extends FSInputController<String?> {
  FSStringInputController(String? value, {super.validator})
      : super(value: value);
}
