import 'package:flutter/widgets.dart';
import 'package:intl/date_symbols.dart';

// symbols extracted and modified from https://github.com/dart-lang/intl/blob/25189230643054f21897302e79cb7e8be862c3ad/lib/date_symbol_data_local.dart
// last commit Aug 27, 2020

// patterns extracted and modified from https://github.com/dart-lang/intl/blob/25189230643054f21897302e79cb7e8be862c3ad/lib/date_time_patterns.dart
// last commit Aug 26, 2020

const deLocale = Locale('de');
final deSymbols = DateSymbols(
  NAME: "de",
  ERAS: const ['v. Chr.', 'n. Chr.'],
  ERANAMES: const ['v. Chr.', 'n. Chr.'],
  NARROWMONTHS: const [
    'J',
    'F',
    'M',
    'A',
    'M',
    'J',
    'J',
    'A',
    'S',
    'O',
    'N',
    'D'
  ],
  STANDALONENARROWMONTHS: const [
    'J',
    'F',
    'M',
    'A',
    'M',
    'J',
    'J',
    'A',
    'S',
    'O',
    'N',
    'D'
  ],
  MONTHS: const [
    'Januar',
    'Februar',
    'März',
    'April',
    'Mai',
    'Juni',
    'Juli',
    'August',
    'September',
    'Oktober',
    'November',
    'Dezember'
  ],
  STANDALONEMONTHS: const [
    'Januar',
    'Februar',
    'März',
    'April',
    'Mai',
    'Juni',
    'Juli',
    'August',
    'September',
    'Oktober',
    'November',
    'Dezember'
  ],
  SHORTMONTHS: const [
    'Jan.',
    'Feb.',
    'März',
    'Apr.',
    'Mai',
    'Juni',
    'Juli',
    'Aug.',
    'Sept.',
    'Okt.',
    'Nov.',
    'Dez.'
  ],
  STANDALONESHORTMONTHS: const [
    'Jan',
    'Feb',
    'Mär',
    'Apr',
    'Mai',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Okt',
    'Nov',
    'Dez'
  ],
  WEEKDAYS: const [
    'Sonntag',
    'Montag',
    'Dienstag',
    'Mittwoch',
    'Donnerstag',
    'Freitag',
    'Samstag'
  ],
  STANDALONEWEEKDAYS: const [
    'Sonntag',
    'Montag',
    'Dienstag',
    'Mittwoch',
    'Donnerstag',
    'Freitag',
    'Samstag'
  ],
  SHORTWEEKDAYS: const ['So.', 'Mo.', 'Di.', 'Mi.', 'Do.', 'Fr.', 'Sa.'],
  STANDALONESHORTWEEKDAYS: const ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
  NARROWWEEKDAYS: const ['S', 'M', 'D', 'M', 'D', 'F', 'S'],
  STANDALONENARROWWEEKDAYS: const ['S', 'M', 'D', 'M', 'D', 'F', 'S'],
  SHORTQUARTERS: const ['Q1', 'Q2', 'Q3', 'Q4'],
  QUARTERS: const ['1. Quartal', '2. Quartal', '3. Quartal', '4. Quartal'],
  AMPMS: const ['AM', 'PM'],
  DATEFORMATS: const ['EEEE, d. MMMM y', 'd. MMMM y', 'dd.MM.y', 'dd.MM.yy'],
  TIMEFORMATS: const ['HH:mm:ss zzzz', 'HH:mm:ss z', 'HH:mm:ss', 'HH:mm'],
  DATETIMEFORMATS: const [
    '{1} \'um\' {0}',
    '{1} \'um\' {0}',
    '{1}, {0}',
    '{1}, {0}'
  ],
  FIRSTDAYOFWEEK: 0,
  WEEKENDRANGE: const [5, 6],
  FIRSTWEEKCUTOFFDAY: 3,
);

const dePatterns = {
  'd': 'dd', // DAY
  'E': 'ccc', // ABBR_WEEKDAY
  'EEEE': 'cccc', // WEEKDAY
  'LLL': 'LLL', // ABBR_STANDALONE_MONTH
  'LLLL': 'LLLL', // STANDALONE_MONTH
  'M': 'LL', // NUM_MONTH
  'Md': 'dd.MM.', // NUM_MONTH_DAY
  'MEd': 'EEE, dd.MM.', // NUM_MONTH_WEEKDAY_DAY
  'MMM': 'LLL', // ABBR_MONTH
  'MMMd': 'd. MMM', // ABBR_MONTH_DAY
  'MMMEd': 'EEE, d. MMM', // ABBR_MONTH_WEEKDAY_DAY
  'MMMM': 'LLLL', // MONTH
  'MMMMd': 'd. MMMM', // MONTH_DAY
  'MMMMEEEEd': 'EEEE, d. MMMM', // MONTH_WEEKDAY_DAY
  'QQQ': 'QQQ', // ABBR_QUARTER
  'QQQQ': 'QQQQ', // QUARTER
  'y': 'yyyy', // YEAR
  'yM': 'MM.yyyy', // YEAR_NUM_MONTH
  'yMd': 'dd.MM.yyyy', // YEAR_NUM_MONTH_DAY
  'yMEd': 'EEE, dd.MM.yyyy', // YEAR_NUM_MONTH_WEEKDAY_DAY
  'yMMM': 'MMM yyyy', // YEAR_ABBR_MONTH
  'yMMMd': 'd. MMM yyyy', // YEAR_ABBR_MONTH_DAY
  'yMMMEd': 'EEE, d. MMM yyyy', // YEAR_ABBR_MONTH_WEEKDAY_DAY
  'yMMMM': 'MMMM yyyy', // YEAR_MONTH
  'yMMMMd': 'd. MMMM yyyy', // YEAR_MONTH_DAY
  'yMMMMEEEEd': 'EEEE, d. MMMM yyyy', // YEAR_MONTH_WEEKDAY_DAY
  'yQQQ': 'QQQ yyyy', // YEAR_ABBR_QUARTER
  'yQQQQ': 'QQQQ yyyy', // YEAR_QUARTER
  'H': 'HH \'Uhr\'', // HOUR24
  'Hm': 'HH:mm', // HOUR24_MINUTE
  'Hms': 'HH:mm:ss', // HOUR24_MINUTE_SECOND
  'j': 'HH \'Uhr\'', // HOUR
  'jm': 'HH:mm', // HOUR_MINUTE
  'jms': 'HH:mm:ss', // HOUR_MINUTE_SECOND
  'jmv': 'HH:mm v', // HOUR_MINUTE_GENERIC_TZ
  'jmz': 'HH:mm z', // HOUR_MINUTETZ
  'jz': 'HH \'Uhr\' z', // HOURGENERIC_TZ
  'm': 'm', // MINUTE
  'ms': 'mm:ss', // MINUTE_SECOND
  's': 's', // SECOND
  'v': 'v', // ABBR_GENERIC_TZ
  'z': 'z', // ABBR_SPECIFIC_TZ
  'zzzz': 'zzzz', // SPECIFIC_TZ
  'ZZZZ': 'ZZZZ' // ABBR_UTC_TZ
};
