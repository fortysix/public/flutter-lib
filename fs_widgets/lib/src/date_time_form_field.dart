import 'package:flutter/material.dart';

import './date.dart';
import './date_form_field.dart';
import './time_form_field.dart';
import '../gen/fs_widget_localizations.dart';

typedef FSDateTimeCallback = void Function(DateTime? date);

class FSDateTimeFormField extends FormField<DateTime?> {
  FSDateTimeFormField({
    super.key,
    this.dateInputKey,
    this.timeInputKey,
    this.dateFocusNode,
    this.timeFocusNode,
    super.initialValue,
    InputDecoration dateDecoration = const InputDecoration(),
    InputDecoration timeDecoration = const InputDecoration(),
    super.onSaved,
    super.validator,
    super.autovalidateMode,
    super.enabled,
    TextInputAction? textInputAction,
    this.onChanged,
    double spacing = 8.0,
    ThemeData? pickerTheme,
  }) : super(builder: (state) {
          state = state as FSDateTimeFormFieldState;
          final theme = Theme.of(state.context);

          final effectiveDateDecoration =
              dateDecoration.applyDefaults(theme.inputDecorationTheme).copyWith(
                    errorText: state.errorText,
                  );
          final effectiveTimeDecoration =
              timeDecoration.applyDefaults(theme.inputDecorationTheme).copyWith(
                    errorText: state.errorText,
                  );

          return Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: FSDateFormField(
                  key: state._dateKey,
                  focusNode: state._actualDateFocusNode,
                  decoration: effectiveDateDecoration,
                  pickerTheme: pickerTheme,
                  autovalidateMode: autovalidateMode,
                  textInputAction: TextInputAction.next,
                  enabled: enabled,
                  initialValue: state.date,
                  onChanged: (value) =>
                      (state as FSDateTimeFormFieldState).date = value,
                  onSaved: (value) =>
                      (state as FSDateTimeFormFieldState)._date = value,
                  validator: (value) {
                    if ((state as FSDateTimeFormFieldState).time != null &&
                        value == null) {
                      return FSWidgetLocalizations.of(state.context)!
                          .formFiledRequired;
                    } else {
                      return null;
                    }
                  },
                  onTodaySet: state._actualTimeFocusNode.requestFocus,
                  onDateSelected: () {
                    (state as FSDateTimeFormFieldState)
                        ._actualTimeFocusNode
                        .requestFocus();
                    state._timeKey.currentState
                        ?.selectTime(state._timeKey.currentState?.labelText);
                  },
                ),
              ),
              SizedBox(width: spacing),
              Expanded(
                child: FSTimeFormField(
                  key: state._timeKey,
                  focusNode: state._actualTimeFocusNode,
                  decoration: effectiveTimeDecoration,
                  pickerTheme: pickerTheme,
                  autovalidateMode: autovalidateMode,
                  textInputAction: textInputAction,
                  enabled: enabled,
                  initialValue: state.time,
                  onNowSet: () {
                    if ((state as FSDateTimeFormFieldState).date == null) {
                      state._dateKey.currentState?.setToday();
                    }
                  },
                  onChanged: (value) =>
                      (state as FSDateTimeFormFieldState).time = value,
                  onSaved: (value) =>
                      (state as FSDateTimeFormFieldState)._time = value,
                  validator: (value) {
                    if ((state as FSDateTimeFormFieldState).date != null &&
                        value == null) {
                      return FSWidgetLocalizations.of(state.context)!
                          .formFiledRequired;
                    } else {
                      return null;
                    }
                  },
                ),
              ),
            ],
          );
        });

  final FSDateTimeCallback? onChanged;
  final GlobalKey<FSDateFormFieldState>? dateInputKey;
  final GlobalKey<FSTimeFormFieldState>? timeInputKey;
  final FocusNode? dateFocusNode;
  final FocusNode? timeFocusNode;

  @override
  FSDateTimeFormFieldState createState() => FSDateTimeFormFieldState();
}

class FSDateTimeFormFieldState extends FormFieldState<DateTime?> {
  late GlobalKey<FSDateFormFieldState> _dateKey;
  late GlobalKey<FSTimeFormFieldState> _timeKey;

  final _dateFieldFocusNode = FocusNode();
  FocusNode get _actualDateFocusNode =>
      widget.dateFocusNode ?? _dateFieldFocusNode;

  final _timeFieldFocusNode = FocusNode();
  FocusNode get _actualTimeFocusNode =>
      widget.timeFocusNode ?? _timeFieldFocusNode;

  @override
  FSDateTimeFormField get widget => super.widget as FSDateTimeFormField;

  FSDate? _date;
  FSDate? get date => _date;
  set date(FSDate? value) {
    _date = value;
    _updateValue(date, time);
  }

  TimeOfDay? _time;
  TimeOfDay? get time => _time;
  set time(TimeOfDay? value) {
    _time = value;
    _updateValue(date, time);
  }

  @override
  void initState() {
    super.initState();

    _dateKey = widget.dateInputKey ?? GlobalKey<FSDateFormFieldState>();
    _timeKey = widget.timeInputKey ?? GlobalKey<FSTimeFormFieldState>();

    _date = widget.initialValue?.toFSDate();
    _time = widget.initialValue != null
        ? TimeOfDay.fromDateTime(widget.initialValue!)
        : null;
  }

  @override
  void didChange(DateTime? value) {
    super.didChange(value);

    if (widget.onChanged != null) {
      widget.onChanged!(value);
    }
  }

  @override
  void setValue(DateTime? value) {
    didChange(value);

    super.setValue(value);
  }

  void updateValue(DateTime? value) {
    _dateKey.currentState?.updateValue(value?.toFSDate());
    _timeKey.currentState
        ?.updateValue(value != null ? TimeOfDay.fromDateTime(value) : null);
  }

  void _updateValue(FSDate? date, TimeOfDay? time) {
    if (date != null && time != null) {
      setValue(
        DateTime(date.year, date.month, date.day, time.hour, time.minute),
      );
    } else {
      setValue(null);
    }
  }
}
