## 0.8.13

* allow clearing values of FSDoubleInputField and FSIntInputField

## 0.8.12

* allow customization of "No entries found" state for FSIntIdInputField and FSStringIdInputField

## 0.8.11

* allow passing setting number format for FSIntInputController
* format inputfield on focus los for FSDoubleInputField, FSIntInputField and FSEuroInputField
* adjust input formatter to use components number format and do not format text for FSDoubleInputField, FSIntInputField and FSEuroInputField

## 0.8.10

* allow selection in disabled state for string, int, double and euro input

## 0.8.9

* allow setting textCapitalization for FSStringInputField
* allow setting keyboardType for FSStringInputField

## 0.8.8

* fix accessing context from within initState for FSDoubleInputField and FSEuroInputField

## 0.8.7

* limit FSDoubleInputField decimal digits to amount of number format
* limit FSEuroInputField decimal digits to amount of number format 

## 0.8.6

* get FSDoubleInputField fallback NumberFormat at runtime respecting used locale
* create FSEuroIputField and FSEuroInputController

## 0.8.5

* fix FSDateInputFields suffic icons not updating on input change

## 0.8.4

* fix FSDateInputField formating text on every input change
* add suffix buttons for today, clear and select dialog to FSDateInputField

## 0.8.3

* Migrate FSDateFormField to FSDateInputField

## 0.8.2

* extend FSDate with toJsonStr and fromJsonStr functions

## 0.8.1

* add enabled parameter to id input fields

## 0.8.0

* bump min flutter version to 3.22.1
* extend FSDate with freezed and json_serializable

## 0.7.13

* allow passing focus node to id input fields

## 0.7.12

* remove TAB as selection step on id input fields
* auto select on focus loss/overlay close with fulltext match or selection
* auto focus first element in list
* highlight focused element with background color

## 0.7.11

* instantly start fetching for id fields on focusing input

## 0.7.10

* dispose id input overlay on widget dispose

## 0.7.9

* add autofocus property to all input fields

## 0.7.8

* add onFieldSubmitted to string, password, int and double input fields

## 0.7.7

* implement password input field

## 0.7.6

* allow setting min input size on int id form field
* allow setting min input size on string id form field

## 0.7.5

* allow dropdown controller to have nullable value
* prevent setState after disposal for int and string id input fields

## 0.7.4

* added FsStringIdInputField
* add autoValidationMode to FsIntIdInputField
* fix validator for FsIntIdInputField
* add autoValidationMode to FsStringInputField

## 0.7.3

* set default of maxLines on FsStringFormField same as default of TextFormField

## 0.7.2

* add minLines and maxLines parameter to FsStringFormField

## 0.7.1

* fix FsIntIdFormField validation error
* fix FsIntIdFormField focus while loading error

## 0.7.0

* add FsIntIdFormField

## 0.6.3

* added enabled to all FsWidgets

## 0.6.2

* added FSDropdownInputField

## 0.6.1

* change version intl dependency to >=0.17.0

## 0.6.0

* updated min dart sdk to 3.0.0
* updated min flutter version to 3.10.0

## 0.5.6

* improve formatting and parsing of double form field

## 0.5.5

* allow intl 0.17.x and 0.18.x

## 0.5.4

* base input form for double added

## 0.5.3

* downgrade intl to ^0.18.0
 
## 0.5.2

* update intl to ^0.18.1

## 0.5.1

* form validators added
* input controllers added
* base input form for string, bool and int added

## 0.5.0

* publication on unpub.fortysix.world

## 0.4.5

* add yyyy-MM-dd as culture independent input format for date form field

## 0.4.4

* add italian translation

## 0.4.3

* add tooltips to icon buttons
* focus time input field after date select in date time form field
* date time form field: allow setting now to set today if date is empty

## 0.4.2

* update form field text before calling on changed

## 0.4.1

* prevent onChange calls on focus loss without changes

## 0.4.0

* updated fs_lints gitlab url

## 0.3.3

* prevent onChanged method calls when saving form on date, time, date time and time duration input fields
 
## 0.3.2

* revert changes from 0.3.1

## 0.3.1

* make _FSIntlOverridesDelegate public

## 0.3.0

* time duration input field
 
## 0.2.3

* fix form field updateValue method to correctly update error state
* prevent single digit minute input on time input field
 
## 0.2.2

* make form field states public and allow updateValue
 
## 0.2.1

* do not limit supported locales of IntlOverrides
* make single digit min input the tenth number on time input field (ex. 8:1 becomes 08:10 not 08:01)
 
## 0.2.0

* FSIntlOverrides delegate added

## 0.1.1

* allow custom picker theme for date, time and date-time widget

## 0.1.0

* date, time and date-time input field
