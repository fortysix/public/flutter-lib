class PseudoEntity {
  final int id;
  final String label;

  const PseudoEntity(this.id, this.label);
}
