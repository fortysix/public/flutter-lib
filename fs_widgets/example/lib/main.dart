import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:fs_widgets/fs_widgets.dart';
import 'package:fs_widgets_example/pseudo_entity.dart';

void main() {
  runApp(const MyApp());
}

final appTheme = ThemeData.dark().copyWith(
  colorScheme: ThemeData.dark().colorScheme.copyWith(
        primary: const Color(0xFF376bb2), // From logo
        secondary: const Color(0xFF8c8d8e), // From logo
      ),
  inputDecorationTheme: ThemeData.dark().inputDecorationTheme.copyWith(
        border: const OutlineInputBorder(),
      ),
);
final pickerTheme = appTheme.copyWith(
  colorScheme: appTheme.colorScheme.copyWith(
    primary: Colors.pink,
    secondary: Colors.orange,
  ),
);

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final locales = <Locale>[];
    if (locales.isEmpty) {
      locales.add(const Locale('de', 'DE'));
    }
    return MaterialApp(
      title: 'Flutter Demo',
      theme: appTheme,
      home: MyHomePage(title: 'fs_widgets: ${locales.first}'),
      // locale: const Locale('en'),
      // supportedLocales: const [Locale('en')],
      // locale: const Locale('de', 'DE'),
      // supportedLocales: const [Locale('de', 'DE')],
      locale: locales.first,
      supportedLocales: locales,
      localizationsDelegates: const [
        ...GlobalMaterialLocalizations.delegates,
        ...FSWidgetLocalizations.localizationsDelegates,
        FSIntlOverrides.delegate,
      ],
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _timeDurationKey = GlobalKey<FSTimeDurationFormFieldState>();
  final _dateKey = GlobalKey<FSDateFormFieldState>();
  final _timeKey = GlobalKey<FSTimeFormFieldState>();
  final _dateTimeKey = GlobalKey<FSDateTimeFormFieldState>();
  final _dateTimeDateFocusNode = FocusNode();
  final _dateTimeTimeFocusNode = FocusNode();

  final Map<int, PseudoEntity> _pseudoEntities = {
    0: PseudoEntity(0, 'Entity A0'),
    1: PseudoEntity(1, 'Entity A1'),
    2: PseudoEntity(2, 'Entity B0'),
    3: PseudoEntity(3, 'Entity B1'),
    4: PseudoEntity(4, 'Entity C0'),
    5: PseudoEntity(5, 'Entity C1'),
    6: PseudoEntity(6, 'Entity C2'),
    7: PseudoEntity(7, 'Entity C3'),
    8: PseudoEntity(8, 'Entity C4'),
    9: PseudoEntity(9, 'Entity C5'),
    10: PseudoEntity(10, 'Entity C6'),
    11: PseudoEntity(11, 'Entity C7'),
    12: PseudoEntity(12, 'Entity C8'),
    13: PseudoEntity(13, 'Entity C9'),
    14: PseudoEntity(14, 'Entity C10'),
    15: PseudoEntity(15, 'Entity C11'),
    16: PseudoEntity(16, 'Entity C12'),
    17: PseudoEntity(17, 'Entity C13'),
  };
  final _intIdController = FSIntIdInputController(null);
  final _intInputController = FSIntInputController(
    null,
    validator: (int? value) {
      if (value == null) {
        return 'Required';
      } else if (value < 3) {
        return 'May not be smaller than 3';
      } else if (value > 10) {
        return 'Mey not be greater than 10';
      } else {
        return null;
      }
    },
  );

  @override
  void initState() {
    super.initState();
    _intIdController.addListener(_printForeignKey);
    _intInputController.addListener(_printIntInput);
  }

  @override
  void dispose() {
    _intIdController.removeListener(_printForeignKey);
    _intInputController.removeListener(_printIntInput);
    _intIdController.dispose();
    _intInputController.dispose();
    super.dispose();
  }

  void _printForeignKey() {
    print('ForeignKey: ${_intIdController.value}');
  }

  void _printIntInput() {
    print('IntInput: ${_intInputController.value}');
  }

  DateTime _now = DateTime.now();
  late Duration _nowDuration = Duration(hours: _now.hour, minutes: _now.minute);

  bool _formValid = true;
  bool _enabled = true;

  Duration? _savedTimeDuration;
  Duration? _changedTimeDuration;

  FSDate? _savedDate;
  FSDate? _changedDate;

  TimeOfDay? _savedTime;
  TimeOfDay? _changedTime;

  DateTime? _savedDateTime;
  DateTime? _changedDateTime;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          Switch(
              value: _enabled,
              onChanged: (value) {
                setState(() {
                  _enabled = value;
                });
              })
        ],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidateMode: AutovalidateMode.always,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Column(
              children: <Widget>[
                const SizedBox(height: 10.0),
                Text(
                  'int input',
                  style: Theme.of(context).primaryTextTheme.titleLarge,
                ),
                const SizedBox(height: 10.0),
                FSIntInputField(
                  controller: _intInputController,
                  decoration: InputDecoration(label: Text('Between 3 and 10')),
                  enabled: _enabled,
                ),
                const SizedBox(height: 10.0),
                Text(
                  'PseudoEntity int id input',
                  style: Theme.of(context).primaryTextTheme.titleLarge,
                ),
                const SizedBox(height: 10.0),
                FSIntIdInputField<PseudoEntity>(
                  controller: _intIdController,
                  decoration: InputDecoration(label: Text('PsuedoForeignKey')),
                  entityToId: (entity) => entity.id,
                  entityToLabel: (entity) => entity.label,
                  fetchEntity: (id) async {
                    await Future.delayed(const Duration(seconds: 1));
                    return _pseudoEntities[id];
                  },
                  queryEntity: (query) async {
                    if (query == null) {
                      return [];
                    }
                    await Future.delayed(const Duration(seconds: 1));
                    return _pseudoEntities.values
                        .where((element) => element.label
                            .toLowerCase()
                            .contains(query.toLowerCase()))
                        .toList(growable: false);
                  },
                  selectEntity: () async {
                    await Future.delayed(const Duration(seconds: 5));
                    return _pseudoEntities[16];
                  },
                ),
                const SizedBox(height: 10.0),
                Row(
                  children: [
                    ElevatedButton(
                      onPressed: () {
                        _intIdController.value = 1;
                      },
                      child: Text('Set id 1'),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        _intIdController.value = 2;
                      },
                      child: Text('Set id 2'),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        _intIdController.value = 3;
                      },
                      child: Text('Set id 3'),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        _intIdController.value = 5;
                      },
                      child: Text('Set id 5'),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        _intIdController.value = 10;
                      },
                      child: Text('Set id 10'),
                    ),
                  ],
                ),
                const SizedBox(height: 10.0),
                Text(
                  'Time duration input',
                  style: Theme.of(context).primaryTextTheme.titleLarge,
                ),
                const SizedBox(height: 10.0),
                FSTimeDurationFormField(
                  key: _timeDurationKey,
                  enabled: _enabled,
                  decoration: const InputDecoration(
                    labelText: 'Wia long?',
                  ),
                  initialValue: _nowDuration,
                  validator: (value) {
                    if (value == null) {
                      return 'Brauchts unbeding';
                    } else {
                      return null;
                    }
                  },
                  onChanged: (value) {
                    setState(() {
                      _changedTimeDuration = value;
                    });
                  },
                  onSaved: (value) {
                    setState(() {
                      _savedTimeDuration = value;
                    });
                  },
                ),
                const SizedBox(height: 10.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: const [
                        Text(
                          'Initial:',
                          textAlign: TextAlign.end,
                        ),
                        Text(
                          'Changed:',
                          textAlign: TextAlign.end,
                        ),
                        Text(
                          'Saved:',
                          textAlign: TextAlign.end,
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '$_nowDuration',
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          '$_changedTimeDuration',
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          '$_savedTimeDuration',
                          textAlign: TextAlign.start,
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 50),
                Text(
                  'Date input',
                  style: Theme.of(context).primaryTextTheme.titleLarge,
                ),
                const SizedBox(height: 10.0),
                FSDateFormField(
                  key: _dateKey,
                  enabled: _enabled,
                  decoration: const InputDecoration(
                    // label: Text('When'),
                    labelText: 'Wenn',
                  ),
                  pickerTheme: pickerTheme,
                  initialValue: _now.toFSDate(),
                  validator: (value) {
                    if (value == null) {
                      return 'Brauchts unbeding';
                    } else {
                      return null;
                    }
                  },
                  onChanged: (value) {
                    setState(() {
                      _changedDate = value;
                    });
                  },
                  onSaved: (value) {
                    setState(() {
                      _savedDate = value;
                    });
                  },
                ),
                const SizedBox(height: 10.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: const [
                        Text(
                          'Initial:',
                          textAlign: TextAlign.end,
                        ),
                        Text(
                          'Changed:',
                          textAlign: TextAlign.end,
                        ),
                        Text(
                          'Saved:',
                          textAlign: TextAlign.end,
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '${_now.toFSDate()}',
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          '$_changedDate',
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          '$_savedDate',
                          textAlign: TextAlign.start,
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '$_now',
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          '${_changedDate?.toDateTime()}',
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          '${_savedDate?.toDateTime()}',
                          textAlign: TextAlign.start,
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 50),
                Text(
                  'Time input',
                  style: Theme.of(context).primaryTextTheme.titleLarge,
                ),
                const SizedBox(height: 10.0),
                FSTimeFormField(
                  key: _timeKey,
                  enabled: _enabled,
                  decoration: const InputDecoration(
                    // label: Text('When'),
                    labelText: 'Wenn',
                  ),
                  pickerTheme: pickerTheme,
                  initialValue: TimeOfDay.fromDateTime(_now),
                  validator: (value) {
                    if (value == null) {
                      return 'Brauchts unbeding';
                    } else {
                      return null;
                    }
                  },
                  onChanged: (value) {
                    setState(() {
                      _changedTime = value;
                    });
                  },
                  onSaved: (value) {
                    setState(() {
                      _savedTime = value;
                    });
                  },
                ),
                const SizedBox(height: 10.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: const [
                        Text(
                          'Initial:',
                          textAlign: TextAlign.end,
                        ),
                        Text(
                          'Changed:',
                          textAlign: TextAlign.end,
                        ),
                        Text(
                          'Saved:',
                          textAlign: TextAlign.end,
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '${TimeOfDay.fromDateTime(_now)}',
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          '$_changedTime',
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          '$_savedTime',
                          textAlign: TextAlign.start,
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '$_now',
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          '${_changedTime?.toDateTime()}',
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          '${_savedTime?.toDateTime()}',
                          textAlign: TextAlign.start,
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 50),
                Text(
                  'DateTime input',
                  style: Theme.of(context).primaryTextTheme.titleLarge,
                ),
                const SizedBox(height: 10.0),
                FSDateTimeFormField(
                  key: _dateTimeKey,
                  dateFocusNode: _dateTimeDateFocusNode,
                  timeFocusNode: _dateTimeTimeFocusNode,
                  enabled: _enabled,
                  dateDecoration: const InputDecoration(
                    labelText: 'Tog',
                  ),
                  pickerTheme: pickerTheme,
                  timeDecoration: const InputDecoration(
                    labelText: 'Zeit',
                  ),
                  initialValue: _now,
                  validator: (value) {
                    if (value == null) {
                      return 'Brauchts unbeding';
                    } else {
                      return null;
                    }
                  },
                  onChanged: (value) {
                    setState(() {
                      _changedDateTime = value;
                    });
                  },
                  onSaved: (value) {
                    setState(() {
                      _savedDateTime = value;
                    });
                  },
                ),
                const SizedBox(height: 10.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: const [
                        Text(
                          'Initial:',
                          textAlign: TextAlign.end,
                        ),
                        Text(
                          'Changed:',
                          textAlign: TextAlign.end,
                        ),
                        Text(
                          'Saved:',
                          textAlign: TextAlign.end,
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '$_now',
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          '$_changedDateTime',
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          '$_savedDateTime',
                          textAlign: TextAlign.start,
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '$_now',
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          '$_changedDateTime',
                          textAlign: TextAlign.start,
                        ),
                        Text(
                          '$_savedDateTime',
                          textAlign: TextAlign.start,
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 50),
                ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState != null) {
                      _formKey.currentState!.save();
                      setState(() {
                        _formValid = _formKey.currentState!.validate();
                      });
                    }
                  },
                  child: const Text('Speichern'),
                ),
                Text('Form valid: $_formValid'),
                const SizedBox(height: 10.0),
                ElevatedButton(
                  onPressed: () {
                    setState(() {
                      _now = DateTime.now();
                      _nowDuration =
                          Duration(hours: _now.hour, minutes: _now.minute);

                      _timeDurationKey.currentState?.updateValue(_nowDuration);
                      _dateKey.currentState?.updateValue(_now.toFSDate());
                      _timeKey.currentState
                          ?.updateValue(TimeOfDay.fromDateTime(_now));
                      _dateTimeKey.currentState?.updateValue(_now);
                    });
                  },
                  child: const Text('Update to now'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
