import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:fs_widgets/fs_widgets.dart';
import 'package:fs_widgets/gen/fs_widget_localizations_de.dart';

void main() {
  group('date time form field', () {
    testWidgets('validator called with error', (tester) async {
      final formKey = GlobalKey<FormState>();
      final inputFieldKey = GlobalKey();
      const dateInputFieldLabel = 'date';
      const timeInputFieldLabel = 'time';
      const requiredError = 'required';
      var changeCompleter = Completer<DateTime?>();
      var saveCompleter = Completer<DateTime?>();

      await tester.pumpWidget(
        MaterialApp(
          locale: const Locale('de', 'DE'),
          supportedLocales: const [Locale('de', 'DE')],
          localizationsDelegates: const [
            ...GlobalMaterialLocalizations.delegates,
            ...FSWidgetLocalizations.localizationsDelegates,
            FSIntlOverrides.delegate,
          ],
          home: Scaffold(
            body: Form(
              key: formKey,
              child: FSDateTimeFormField(
                key: inputFieldKey,
                dateDecoration:
                    const InputDecoration(labelText: dateInputFieldLabel),
                timeDecoration:
                    const InputDecoration(label: Text(timeInputFieldLabel)),
                validator: (value) {
                  if (value == null) {
                    return requiredError;
                  } else {
                    return null;
                  }
                },
                onChanged: (value) {
                  if (changeCompleter.isCompleted) {
                    changeCompleter = Completer<DateTime?>();
                  }
                  changeCompleter.complete(value);
                },
                onSaved: (value) {
                  if (saveCompleter.isCompleted) {
                    saveCompleter = Completer<DateTime?>();
                  }
                  saveCompleter.complete(value);
                },
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();

      final dateLabelFinder = find.text(dateInputFieldLabel);
      final timeLabelFinder = find.text(timeInputFieldLabel);
      final requiredErrorFinder = find.text(requiredError);
      final inputFinder = find.byKey(inputFieldKey);
      final invalidTimeErrorFinder =
          find.text(FSWidgetLocalizationsDe().timeFormFieldInvalidFormat);

      expect(inputFinder, findsOneWidget);
      expect(dateLabelFinder, findsOneWidget);
      expect(timeLabelFinder, findsOneWidget);
      expect(requiredErrorFinder, findsNothing);
      expect(invalidTimeErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, false);

      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), false);
      await tester.pumpAndSettle();
      expect(requiredErrorFinder, findsNWidgets(2));
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, true);
      expect(await saveCompleter.future, equals(null));
    });

    testWidgets('invalid date input', (tester) async {
      final formKey = GlobalKey<FormState>();
      final inputFieldKey = GlobalKey();
      final dateInputFieldKey = GlobalKey<FSDateFormFieldState>();
      final timeInputFieldKey = GlobalKey<FSTimeFormFieldState>();
      const dateInputFieldLabel = 'date';
      const timeInputFieldLabel = 'time';
      var changeCompleter = Completer<DateTime?>();
      var saveCompleter = Completer<DateTime?>();

      await tester.pumpWidget(
        MaterialApp(
          locale: const Locale('de', 'DE'),
          supportedLocales: const [Locale('de', 'DE')],
          localizationsDelegates: const [
            ...GlobalMaterialLocalizations.delegates,
            ...FSWidgetLocalizations.localizationsDelegates,
            FSIntlOverrides.delegate,
          ],
          home: Scaffold(
            body: Form(
              key: formKey,
              child: FSDateTimeFormField(
                key: inputFieldKey,
                dateInputKey: dateInputFieldKey,
                timeInputKey: timeInputFieldKey,
                dateDecoration:
                    const InputDecoration(labelText: dateInputFieldLabel),
                timeDecoration:
                    const InputDecoration(label: Text(timeInputFieldLabel)),
                onChanged: (value) {
                  if (changeCompleter.isCompleted) {
                    changeCompleter = Completer<DateTime?>();
                  }
                  changeCompleter.complete(value);
                },
                onSaved: (value) {
                  if (saveCompleter.isCompleted) {
                    saveCompleter = Completer<DateTime?>();
                  }
                  saveCompleter.complete(value);
                },
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();

      final dateLabelFinder = find.text(dateInputFieldLabel);
      final timeLabelFinder = find.text(timeInputFieldLabel);
      final inputFinder = find.byKey(inputFieldKey);
      final dateInputFinder = find.byKey(dateInputFieldKey);
      final timeInputFinder = find.byKey(timeInputFieldKey);
      final invalidDateErrorFinder =
          find.text(FSWidgetLocalizationsDe().dateFormFieldInvalidFormat);

      expect(inputFinder, findsOneWidget);
      expect(dateInputFinder, findsOneWidget);
      expect(timeInputFinder, findsOneWidget);
      expect(dateLabelFinder, findsOneWidget);
      expect(timeLabelFinder, findsOneWidget);
      expect(invalidDateErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, false);

      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(invalidDateErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, true);
      expect(await saveCompleter.future, equals(null));

      await tester.enterText(dateInputFinder, 'asdf');
      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), false);
      await tester.pumpAndSettle();
      expect(invalidDateErrorFinder, findsOneWidget);
      expect(changeCompleter.isCompleted, true);
      expect(saveCompleter.isCompleted, true);
      expect(await changeCompleter.future, equals(null));
      expect(await saveCompleter.future, equals(null));
    });

    testWidgets('invalid time input', (tester) async {
      final formKey = GlobalKey<FormState>();
      final inputFieldKey = GlobalKey();
      final dateInputFieldKey = GlobalKey<FSDateFormFieldState>();
      final timeInputFieldKey = GlobalKey<FSTimeFormFieldState>();
      const dateInputFieldLabel = 'date';
      const timeInputFieldLabel = 'time';
      var changeCompleter = Completer<DateTime?>();
      var saveCompleter = Completer<DateTime?>();

      await tester.pumpWidget(
        MaterialApp(
          locale: const Locale('de', 'DE'),
          supportedLocales: const [Locale('de', 'DE')],
          localizationsDelegates: const [
            ...GlobalMaterialLocalizations.delegates,
            ...FSWidgetLocalizations.localizationsDelegates,
            FSIntlOverrides.delegate,
          ],
          home: Scaffold(
            body: Form(
              key: formKey,
              child: FSDateTimeFormField(
                key: inputFieldKey,
                dateInputKey: dateInputFieldKey,
                timeInputKey: timeInputFieldKey,
                dateDecoration:
                    const InputDecoration(labelText: dateInputFieldLabel),
                timeDecoration:
                    const InputDecoration(label: Text(timeInputFieldLabel)),
                onChanged: (value) {
                  if (changeCompleter.isCompleted) {
                    changeCompleter = Completer<DateTime?>();
                  }
                  changeCompleter.complete(value);
                },
                onSaved: (value) {
                  if (saveCompleter.isCompleted) {
                    saveCompleter = Completer<DateTime?>();
                  }
                  saveCompleter.complete(value);
                },
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();

      final dateLabelFinder = find.text(dateInputFieldLabel);
      final timeLabelFinder = find.text(timeInputFieldLabel);
      final inputFinder = find.byKey(inputFieldKey);
      final dateInputFinder = find.byKey(dateInputFieldKey);
      final timeInputFinder = find.byKey(timeInputFieldKey);
      final invalidTimeErrorFinder =
          find.text(FSWidgetLocalizationsDe().timeFormFieldInvalidFormat);

      expect(inputFinder, findsOneWidget);
      expect(dateInputFinder, findsOneWidget);
      expect(timeInputFinder, findsOneWidget);
      expect(dateLabelFinder, findsOneWidget);
      expect(timeLabelFinder, findsOneWidget);
      expect(invalidTimeErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, false);

      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(invalidTimeErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, true);
      expect(await saveCompleter.future, equals(null));

      await tester.enterText(timeInputFinder, '15:3');
      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), false);
      await tester.pumpAndSettle();
      expect(invalidTimeErrorFinder, findsOneWidget);
      expect(changeCompleter.isCompleted, true);
      expect(saveCompleter.isCompleted, true);
      expect(await changeCompleter.future, equals(null));
      expect(await saveCompleter.future, equals(null));
    });

    testWidgets('valid date input', (tester) async {
      final formKey = GlobalKey<FormState>();
      final inputFieldKey = GlobalKey();
      final dateInputFieldKey = GlobalKey<FSDateFormFieldState>();
      final timeInputFieldKey = GlobalKey<FSTimeFormFieldState>();
      const dateInputFieldLabel = 'date';
      const timeInputFieldLabel = 'time';
      var changeCompleter = Completer<DateTime?>();
      var saveCompleter = Completer<DateTime?>();

      await tester.pumpWidget(
        MaterialApp(
          locale: const Locale('de', 'DE'),
          supportedLocales: const [Locale('de', 'DE')],
          localizationsDelegates: const [
            ...GlobalMaterialLocalizations.delegates,
            ...FSWidgetLocalizations.localizationsDelegates,
            FSIntlOverrides.delegate,
          ],
          home: Scaffold(
            body: Form(
              key: formKey,
              child: FSDateTimeFormField(
                key: inputFieldKey,
                dateInputKey: dateInputFieldKey,
                timeInputKey: timeInputFieldKey,
                dateDecoration:
                    const InputDecoration(labelText: dateInputFieldLabel),
                timeDecoration:
                    const InputDecoration(label: Text(timeInputFieldLabel)),
                onChanged: (value) {
                  if (changeCompleter.isCompleted) {
                    changeCompleter = Completer<DateTime?>();
                  }
                  changeCompleter.complete(value);
                },
                onSaved: (value) {
                  if (saveCompleter.isCompleted) {
                    saveCompleter = Completer<DateTime?>();
                  }
                  saveCompleter.complete(value);
                },
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();

      final dateLabelFinder = find.text(dateInputFieldLabel);
      final timeLabelFinder = find.text(timeInputFieldLabel);
      final inputFinder = find.byKey(inputFieldKey);
      final dateInputFinder = find.byKey(dateInputFieldKey);
      final timeInputFinder = find.byKey(timeInputFieldKey);
      final requiredErrorFinder =
          find.text(FSWidgetLocalizationsDe().formFiledRequired);

      expect(inputFinder, findsOneWidget);
      expect(dateInputFinder, findsOneWidget);
      expect(timeInputFinder, findsOneWidget);
      expect(dateLabelFinder, findsOneWidget);
      expect(timeLabelFinder, findsOneWidget);
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, false);

      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, true);
      expect(await saveCompleter.future, equals(null));

      await tester.enterText(dateInputFinder, '10.02.2023');
      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), false);
      await tester.pumpAndSettle();
      expect(requiredErrorFinder, findsOneWidget);
      expect(changeCompleter.isCompleted, true);
      expect(saveCompleter.isCompleted, true);
      expect(await changeCompleter.future, equals(null));
      expect(await saveCompleter.future, equals(null));
    });

    testWidgets('valid time input', (tester) async {
      final formKey = GlobalKey<FormState>();
      final inputFieldKey = GlobalKey();
      final dateInputFieldKey = GlobalKey<FSDateFormFieldState>();
      final timeInputFieldKey = GlobalKey<FSTimeFormFieldState>();
      const dateInputFieldLabel = 'date';
      const timeInputFieldLabel = 'time';
      var changeCompleter = Completer<DateTime?>();
      var saveCompleter = Completer<DateTime?>();

      await tester.pumpWidget(
        MaterialApp(
          locale: const Locale('de', 'DE'),
          supportedLocales: const [Locale('de', 'DE')],
          localizationsDelegates: const [
            ...GlobalMaterialLocalizations.delegates,
            ...FSWidgetLocalizations.localizationsDelegates,
            FSIntlOverrides.delegate,
          ],
          home: Scaffold(
            body: Form(
              key: formKey,
              child: FSDateTimeFormField(
                key: inputFieldKey,
                dateInputKey: dateInputFieldKey,
                timeInputKey: timeInputFieldKey,
                dateDecoration:
                    const InputDecoration(labelText: dateInputFieldLabel),
                timeDecoration:
                    const InputDecoration(label: Text(timeInputFieldLabel)),
                onChanged: (value) {
                  if (changeCompleter.isCompleted) {
                    changeCompleter = Completer<DateTime?>();
                  }
                  changeCompleter.complete(value);
                },
                onSaved: (value) {
                  if (saveCompleter.isCompleted) {
                    saveCompleter = Completer<DateTime?>();
                  }
                  saveCompleter.complete(value);
                },
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();

      final dateLabelFinder = find.text(dateInputFieldLabel);
      final timeLabelFinder = find.text(timeInputFieldLabel);
      final inputFinder = find.byKey(inputFieldKey);
      final dateInputFinder = find.byKey(dateInputFieldKey);
      final timeInputFinder = find.byKey(timeInputFieldKey);
      final requiredErrorFinder =
          find.text(FSWidgetLocalizationsDe().formFiledRequired);

      expect(inputFinder, findsOneWidget);
      expect(dateInputFinder, findsOneWidget);
      expect(timeInputFinder, findsOneWidget);
      expect(dateLabelFinder, findsOneWidget);
      expect(timeLabelFinder, findsOneWidget);
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, false);

      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, true);
      expect(await saveCompleter.future, equals(null));

      await tester.enterText(timeInputFinder, '10:30');
      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), false);
      await tester.pumpAndSettle();
      expect(requiredErrorFinder, findsOneWidget);
      expect(changeCompleter.isCompleted, true);
      expect(saveCompleter.isCompleted, true);
      expect(await changeCompleter.future, equals(null));
      expect(await saveCompleter.future, equals(null));
    });

    testWidgets('valid input', (tester) async {
      final formKey = GlobalKey<FormState>();
      final inputFieldKey = GlobalKey();
      final dateInputFieldKey = GlobalKey<FSDateFormFieldState>();
      final timeInputFieldKey = GlobalKey<FSTimeFormFieldState>();
      const dateInputFieldLabel = 'date';
      const timeInputFieldLabel = 'time';
      var changeCompleter = Completer<DateTime?>();
      var saveCompleter = Completer<DateTime?>();

      await tester.pumpWidget(
        MaterialApp(
          locale: const Locale('de', 'DE'),
          supportedLocales: const [Locale('de', 'DE')],
          localizationsDelegates: const [
            ...GlobalMaterialLocalizations.delegates,
            ...FSWidgetLocalizations.localizationsDelegates,
            FSIntlOverrides.delegate,
          ],
          home: Scaffold(
            body: Form(
              key: formKey,
              child: FSDateTimeFormField(
                key: inputFieldKey,
                dateInputKey: dateInputFieldKey,
                timeInputKey: timeInputFieldKey,
                dateDecoration:
                    const InputDecoration(labelText: dateInputFieldLabel),
                timeDecoration:
                    const InputDecoration(label: Text(timeInputFieldLabel)),
                onChanged: (value) {
                  if (changeCompleter.isCompleted) {
                    changeCompleter = Completer<DateTime?>();
                  }
                  changeCompleter.complete(value);
                },
                onSaved: (value) {
                  if (saveCompleter.isCompleted) {
                    saveCompleter = Completer<DateTime?>();
                  }
                  saveCompleter.complete(value);
                },
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();

      final dateLabelFinder = find.text(dateInputFieldLabel);
      final timeLabelFinder = find.text(timeInputFieldLabel);
      final inputFinder = find.byKey(inputFieldKey);
      final dateInputFinder = find.byKey(dateInputFieldKey);
      final timeInputFinder = find.byKey(timeInputFieldKey);
      final requiredErrorFinder =
          find.text(FSWidgetLocalizationsDe().formFiledRequired);

      expect(inputFinder, findsOneWidget);
      expect(dateInputFinder, findsOneWidget);
      expect(timeInputFinder, findsOneWidget);
      expect(dateLabelFinder, findsOneWidget);
      expect(timeLabelFinder, findsOneWidget);
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, false);

      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, true);
      expect(await saveCompleter.future, equals(null));

      final resultValue = DateTime(2023, 2, 20, 10, 22);
      await tester.enterText(dateInputFinder, '20.2.23');
      await tester.enterText(timeInputFinder, '10:22');
      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, true);
      expect(saveCompleter.isCompleted, true);
      expect(await changeCompleter.future, equals(resultValue));
      expect(await saveCompleter.future, equals(resultValue));
    });

    testWidgets('with initial input', (tester) async {
      final formKey = GlobalKey<FormState>();
      final inputFieldKey = GlobalKey();
      final dateInputFieldKey = GlobalKey<FSDateFormFieldState>();
      final timeInputFieldKey = GlobalKey<FSTimeFormFieldState>();
      const dateInputFieldLabel = 'date';
      const timeInputFieldLabel = 'time';
      const requiredError = 'required';
      var changeCompleter = Completer<DateTime?>();
      var saveCompleter = Completer<DateTime?>();
      final initialValue = DateTime(2022, 11, 15, 6, 59);

      await tester.pumpWidget(
        MaterialApp(
          locale: const Locale('de', 'DE'),
          supportedLocales: const [Locale('de', 'DE')],
          localizationsDelegates: const [
            ...GlobalMaterialLocalizations.delegates,
            ...FSWidgetLocalizations.localizationsDelegates,
            FSIntlOverrides.delegate,
          ],
          home: Scaffold(
            body: Form(
              key: formKey,
              child: FSDateTimeFormField(
                key: inputFieldKey,
                dateInputKey: dateInputFieldKey,
                timeInputKey: timeInputFieldKey,
                dateDecoration:
                    const InputDecoration(labelText: dateInputFieldLabel),
                timeDecoration:
                    const InputDecoration(label: Text(timeInputFieldLabel)),
                initialValue: initialValue,
                validator: (value) {
                  if (value == null) {
                    return requiredError;
                  } else {
                    return null;
                  }
                },
                onChanged: (value) {
                  if (changeCompleter.isCompleted) {
                    changeCompleter = Completer<DateTime?>();
                  }
                  changeCompleter.complete(value);
                },
                onSaved: (value) {
                  if (saveCompleter.isCompleted) {
                    saveCompleter = Completer<DateTime?>();
                  }
                  saveCompleter.complete(value);
                },
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();

      final dateLabelFinder = find.text(dateInputFieldLabel);
      final timeLabelFinder = find.text(timeInputFieldLabel);
      final requiredErrorFinder = find.text(requiredError);
      final inputFinder = find.byKey(inputFieldKey);
      final dateInputFinder = find.byKey(dateInputFieldKey);
      final timeInputFinder = find.byKey(timeInputFieldKey);

      expect(inputFinder, findsOneWidget);
      expect(dateInputFinder, findsOneWidget);
      expect(timeInputFinder, findsOneWidget);
      expect(dateLabelFinder, findsOneWidget);
      expect(timeLabelFinder, findsOneWidget);
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, false);

      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, true);
      expect(await saveCompleter.future, equals(initialValue));

      final resultValue = DateTime(2024, 12, 24, 0, 0);
      await tester.enterText(dateInputFinder, '24.12.2024');
      await tester.enterText(timeInputFinder, '0:00');
      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, true);
      expect(saveCompleter.isCompleted, true);
      expect(await changeCompleter.future, equals(resultValue));
      expect(await saveCompleter.future, equals(resultValue));
    });

    testWidgets('quick fill today then now', (tester) async {
      final formKey = GlobalKey<FormState>();
      final inputFieldKey = GlobalKey();
      final dateInputFieldKey = GlobalKey<FSDateFormFieldState>();
      final timeInputFieldKey = GlobalKey<FSTimeFormFieldState>();
      final dateFocusNode = FocusNode();
      final timeFocusNode = FocusNode();
      const dateInputFieldLabel = 'date';
      const timeInputFieldLabel = 'time';
      var changeCompleter = Completer<DateTime?>();
      var saveCompleter = Completer<DateTime?>();

      await tester.pumpWidget(
        MaterialApp(
          locale: const Locale('de', 'DE'),
          supportedLocales: const [Locale('de', 'DE')],
          localizationsDelegates: const [
            ...GlobalMaterialLocalizations.delegates,
            ...FSWidgetLocalizations.localizationsDelegates,
            FSIntlOverrides.delegate,
          ],
          home: Scaffold(
            body: Form(
              key: formKey,
              child: FSDateTimeFormField(
                key: inputFieldKey,
                dateInputKey: dateInputFieldKey,
                timeInputKey: timeInputFieldKey,
                dateFocusNode: dateFocusNode,
                timeFocusNode: timeFocusNode,
                dateDecoration:
                    const InputDecoration(labelText: dateInputFieldLabel),
                timeDecoration:
                    const InputDecoration(label: Text(timeInputFieldLabel)),
                onChanged: (value) {
                  if (changeCompleter.isCompleted) {
                    changeCompleter = Completer<DateTime?>();
                  }
                  changeCompleter.complete(value);
                },
                onSaved: (value) {
                  if (saveCompleter.isCompleted) {
                    saveCompleter = Completer<DateTime?>();
                  }
                  saveCompleter.complete(value);
                },
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();

      final dateLabelFinder = find.text(dateInputFieldLabel);
      final timeLabelFinder = find.text(timeInputFieldLabel);
      final inputFinder = find.byKey(inputFieldKey);
      final dateInputFinder = find.byKey(dateInputFieldKey);
      final timeInputFinder = find.byKey(timeInputFieldKey);
      final requiredErrorFinder =
          find.text(FSWidgetLocalizationsDe().formFiledRequired);
      final todayIconFinder = find.byIcon(Icons.today);
      final nowIconFinder = find.byIcon(Icons.access_time);

      expect(inputFinder, findsOneWidget);
      expect(dateInputFinder, findsOneWidget);
      expect(timeInputFinder, findsOneWidget);
      expect(dateLabelFinder, findsOneWidget);
      expect(timeLabelFinder, findsOneWidget);
      expect(todayIconFinder, findsOneWidget);
      expect(nowIconFinder, findsOneWidget);
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, false);

      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, true);
      expect(await saveCompleter.future, equals(null));

      await tester.tap(todayIconFinder);
      expect(timeFocusNode.hasFocus, true);
      await tester.tap(nowIconFinder);

      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, true);
      expect(saveCompleter.isCompleted, true);
    });

    testWidgets('quick fill now with empty date', (tester) async {
      final formKey = GlobalKey<FormState>();
      final inputFieldKey = GlobalKey();
      final dateInputFieldKey = GlobalKey<FSDateFormFieldState>();
      final timeInputFieldKey = GlobalKey<FSTimeFormFieldState>();
      final dateFocusNode = FocusNode();
      final timeFocusNode = FocusNode();
      const dateInputFieldLabel = 'date';
      const timeInputFieldLabel = 'time';
      var changeCompleter = Completer<DateTime?>();
      var saveCompleter = Completer<DateTime?>();

      await tester.pumpWidget(
        MaterialApp(
          locale: const Locale('de', 'DE'),
          supportedLocales: const [Locale('de', 'DE')],
          localizationsDelegates: const [
            ...GlobalMaterialLocalizations.delegates,
            ...FSWidgetLocalizations.localizationsDelegates,
            FSIntlOverrides.delegate,
          ],
          home: Scaffold(
            body: Form(
              key: formKey,
              child: FSDateTimeFormField(
                key: inputFieldKey,
                dateInputKey: dateInputFieldKey,
                timeInputKey: timeInputFieldKey,
                dateFocusNode: dateFocusNode,
                timeFocusNode: timeFocusNode,
                dateDecoration:
                    const InputDecoration(labelText: dateInputFieldLabel),
                timeDecoration:
                    const InputDecoration(label: Text(timeInputFieldLabel)),
                onChanged: (value) {
                  if (changeCompleter.isCompleted) {
                    changeCompleter = Completer<DateTime?>();
                  }
                  changeCompleter.complete(value);
                },
                onSaved: (value) {
                  if (saveCompleter.isCompleted) {
                    saveCompleter = Completer<DateTime?>();
                  }
                  saveCompleter.complete(value);
                },
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();

      final dateLabelFinder = find.text(dateInputFieldLabel);
      final timeLabelFinder = find.text(timeInputFieldLabel);
      final inputFinder = find.byKey(inputFieldKey);
      final dateInputFinder = find.byKey(dateInputFieldKey);
      final timeInputFinder = find.byKey(timeInputFieldKey);
      final requiredErrorFinder =
          find.text(FSWidgetLocalizationsDe().formFiledRequired);
      final todayIconFinder = find.byIcon(Icons.today);
      final nowIconFinder = find.byIcon(Icons.access_time);

      expect(inputFinder, findsOneWidget);
      expect(dateInputFinder, findsOneWidget);
      expect(timeInputFinder, findsOneWidget);
      expect(dateLabelFinder, findsOneWidget);
      expect(timeLabelFinder, findsOneWidget);
      expect(todayIconFinder, findsOneWidget);
      expect(nowIconFinder, findsOneWidget);
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, false);

      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, true);
      expect(await saveCompleter.future, equals(null));

      await tester.tap(nowIconFinder);

      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, true);
      expect(saveCompleter.isCompleted, true);
    });

    testWidgets('quick fill now with valid date', (tester) async {
      final formKey = GlobalKey<FormState>();
      final inputFieldKey = GlobalKey();
      final dateInputFieldKey = GlobalKey<FSDateFormFieldState>();
      final timeInputFieldKey = GlobalKey<FSTimeFormFieldState>();
      final dateFocusNode = FocusNode();
      final timeFocusNode = FocusNode();
      const dateInputFieldLabel = 'date';
      const timeInputFieldLabel = 'time';
      var changeCompleter = Completer<DateTime?>();
      var saveCompleter = Completer<DateTime?>();

      await tester.pumpWidget(
        MaterialApp(
          locale: const Locale('de', 'DE'),
          supportedLocales: const [Locale('de', 'DE')],
          localizationsDelegates: const [
            ...GlobalMaterialLocalizations.delegates,
            ...FSWidgetLocalizations.localizationsDelegates,
            FSIntlOverrides.delegate,
          ],
          home: Scaffold(
            body: Form(
              key: formKey,
              child: FSDateTimeFormField(
                key: inputFieldKey,
                dateInputKey: dateInputFieldKey,
                timeInputKey: timeInputFieldKey,
                dateFocusNode: dateFocusNode,
                timeFocusNode: timeFocusNode,
                dateDecoration:
                    const InputDecoration(labelText: dateInputFieldLabel),
                timeDecoration:
                    const InputDecoration(label: Text(timeInputFieldLabel)),
                onChanged: (value) {
                  if (changeCompleter.isCompleted) {
                    changeCompleter = Completer<DateTime?>();
                  }
                  changeCompleter.complete(value);
                },
                onSaved: (value) {
                  if (saveCompleter.isCompleted) {
                    saveCompleter = Completer<DateTime?>();
                  }
                  saveCompleter.complete(value);
                },
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();

      final dateLabelFinder = find.text(dateInputFieldLabel);
      final timeLabelFinder = find.text(timeInputFieldLabel);
      final inputFinder = find.byKey(inputFieldKey);
      final dateInputFinder = find.byKey(dateInputFieldKey);
      final timeInputFinder = find.byKey(timeInputFieldKey);
      final requiredErrorFinder =
          find.text(FSWidgetLocalizationsDe().formFiledRequired);
      final todayIconFinder = find.byIcon(Icons.today);
      final nowIconFinder = find.byIcon(Icons.access_time);

      expect(inputFinder, findsOneWidget);
      expect(dateInputFinder, findsOneWidget);
      expect(timeInputFinder, findsOneWidget);
      expect(dateLabelFinder, findsOneWidget);
      expect(timeLabelFinder, findsOneWidget);
      expect(todayIconFinder, findsOneWidget);
      expect(nowIconFinder, findsOneWidget);
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, false);

      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, true);
      expect(await saveCompleter.future, equals(null));

      const resultValue = FSDate(2023, 4, 24);
      await tester.enterText(dateInputFinder, '24.04.2023');
      await tester.tap(nowIconFinder);
      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, true);
      expect(saveCompleter.isCompleted, true);
      expect((await changeCompleter.future)?.toFSDate(), equals(resultValue));
    });
  });
}
