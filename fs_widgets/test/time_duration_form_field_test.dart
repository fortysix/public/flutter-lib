import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:fs_widgets/fs_widgets.dart';
import 'package:fs_widgets/gen/fs_widget_localizations_de.dart';

void main() {
  group('time duration form field', () {
    testWidgets('validator called with error', (tester) async {
      final formKey = GlobalKey<FormState>();
      final inputFieldKey = GlobalKey();
      const inputFieldLabel = 'time duration';
      const requiredError = 'required';
      var changeCompleter = Completer<Duration?>();
      var saveCompleter = Completer<Duration?>();

      await tester.pumpWidget(
        MaterialApp(
          locale: const Locale('de', 'DE'),
          supportedLocales: const [Locale('de', 'DE')],
          localizationsDelegates: const [
            ...GlobalMaterialLocalizations.delegates,
            ...FSWidgetLocalizations.localizationsDelegates,
            FSIntlOverrides.delegate,
          ],
          home: Scaffold(
            body: Form(
              key: formKey,
              child: FSTimeDurationFormField(
                key: inputFieldKey,
                decoration: const InputDecoration(labelText: inputFieldLabel),
                validator: (value) {
                  if (value == null) {
                    return requiredError;
                  } else {
                    return null;
                  }
                },
                onChanged: (value) {
                  if (changeCompleter.isCompleted) {
                    changeCompleter = Completer<Duration?>();
                  }
                  changeCompleter.complete(value);
                },
                onSaved: (value) {
                  if (saveCompleter.isCompleted) {
                    saveCompleter = Completer<Duration?>();
                  }
                  saveCompleter.complete(value);
                },
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();

      final labelFinder = find.text(inputFieldLabel);
      final requiredErrorFinder = find.text(requiredError);
      final inputFinder = find.byKey(inputFieldKey);
      final invalidTimeErrorFinder =
          find.text(FSWidgetLocalizationsDe().timeFormFieldInvalidFormat);

      expect(inputFinder, findsOneWidget);
      expect(labelFinder, findsOneWidget);
      expect(requiredErrorFinder, findsNothing);
      expect(invalidTimeErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, false);

      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), false);
      await tester.pumpAndSettle();
      expect(requiredErrorFinder, findsOneWidget);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, true);
      expect(await saveCompleter.future, equals(null));
    });

    testWidgets('invalid input', (tester) async {
      final formKey = GlobalKey<FormState>();
      final inputFieldKey = GlobalKey();
      const inputFieldLabel = 'date';
      var changeCompleter = Completer<Duration?>();
      var saveCompleter = Completer<Duration?>();

      await tester.pumpWidget(
        MaterialApp(
          locale: const Locale('de', 'DE'),
          supportedLocales: const [Locale('de', 'DE')],
          localizationsDelegates: const [
            ...GlobalMaterialLocalizations.delegates,
            ...FSWidgetLocalizations.localizationsDelegates,
            FSIntlOverrides.delegate,
          ],
          home: Scaffold(
            body: Form(
              key: formKey,
              child: FSTimeDurationFormField(
                key: inputFieldKey,
                decoration: const InputDecoration(labelText: inputFieldLabel),
                onChanged: (value) {
                  if (changeCompleter.isCompleted) {
                    changeCompleter = Completer<Duration?>();
                  }
                  changeCompleter.complete(value);
                },
                onSaved: (value) {
                  if (saveCompleter.isCompleted) {
                    saveCompleter = Completer<Duration?>();
                  }
                  saveCompleter.complete(value);
                },
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();

      final labelFinder = find.text(inputFieldLabel);
      final inputFinder = find.byKey(inputFieldKey);
      final invalidTimeErrorFinder =
          find.text(FSWidgetLocalizationsDe().timeFormFieldInvalidFormat);

      expect(inputFinder, findsOneWidget);
      expect(labelFinder, findsOneWidget);
      expect(invalidTimeErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, false);

      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, true);
      expect(await saveCompleter.future, equals(null));

      await tester.enterText(inputFinder, '111:5');
      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), false);
      await tester.pumpAndSettle();
      expect(invalidTimeErrorFinder, findsOneWidget);
      expect(changeCompleter.isCompleted, true);
      expect(saveCompleter.isCompleted, true);
      expect(await changeCompleter.future, equals(null));
      expect(await saveCompleter.future, equals(null));
    });

    testWidgets('valid input', (tester) async {
      final formKey = GlobalKey<FormState>();
      final inputFieldKey = GlobalKey();
      const inputFieldLabel = 'date';
      var changeCompleter = Completer<Duration?>();
      var saveCompleter = Completer<Duration?>();

      await tester.pumpWidget(
        MaterialApp(
          locale: const Locale('de', 'DE'),
          supportedLocales: const [Locale('de', 'DE')],
          localizationsDelegates: const [
            ...GlobalMaterialLocalizations.delegates,
            ...FSWidgetLocalizations.localizationsDelegates,
            FSIntlOverrides.delegate,
          ],
          home: Scaffold(
            body: Form(
              key: formKey,
              child: FSTimeDurationFormField(
                key: inputFieldKey,
                decoration: const InputDecoration(labelText: inputFieldLabel),
                onChanged: (value) {
                  if (changeCompleter.isCompleted) {
                    changeCompleter = Completer<Duration?>();
                  }
                  changeCompleter.complete(value);
                },
                onSaved: (value) {
                  if (saveCompleter.isCompleted) {
                    saveCompleter = Completer<Duration?>();
                  }
                  saveCompleter.complete(value);
                },
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();

      final labelFinder = find.text(inputFieldLabel);
      final inputFinder = find.byKey(inputFieldKey);
      final invalidTimeErrorFinder =
          find.text(FSWidgetLocalizationsDe().timeFormFieldInvalidFormat);

      expect(inputFinder, findsOneWidget);
      expect(labelFinder, findsOneWidget);
      expect(invalidTimeErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, false);

      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, true);
      expect(await saveCompleter.future, equals(null));

      const resultDate = Duration(hours: 89, minutes: 50);
      await tester.enterText(inputFinder, '89:50');
      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(invalidTimeErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, true);
      expect(saveCompleter.isCompleted, true);
      expect(await changeCompleter.future, equals(resultDate));
      expect(await saveCompleter.future, equals(resultDate));
    });

    testWidgets('valid number input', (tester) async {
      final formKey = GlobalKey<FormState>();
      final inputFieldKey = GlobalKey();
      const inputFieldLabel = 'date';
      var changeCompleter = Completer<Duration?>();
      var saveCompleter = Completer<Duration?>();

      await tester.pumpWidget(
        MaterialApp(
          locale: const Locale('de', 'DE'),
          supportedLocales: const [Locale('de', 'DE')],
          localizationsDelegates: const [
            ...GlobalMaterialLocalizations.delegates,
            ...FSWidgetLocalizations.localizationsDelegates,
            FSIntlOverrides.delegate,
          ],
          home: Scaffold(
            body: Form(
              key: formKey,
              child: FSTimeDurationFormField(
                key: inputFieldKey,
                decoration: const InputDecoration(labelText: inputFieldLabel),
                onChanged: (value) {
                  if (changeCompleter.isCompleted) {
                    changeCompleter = Completer<Duration?>();
                  }
                  changeCompleter.complete(value);
                },
                onSaved: (value) {
                  if (saveCompleter.isCompleted) {
                    saveCompleter = Completer<Duration?>();
                  }
                  saveCompleter.complete(value);
                },
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();

      final labelFinder = find.text(inputFieldLabel);
      final inputFinder = find.byKey(inputFieldKey);
      final invalidTimeErrorFinder =
          find.text(FSWidgetLocalizationsDe().timeFormFieldInvalidFormat);

      expect(inputFinder, findsOneWidget);
      expect(labelFinder, findsOneWidget);
      expect(invalidTimeErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, false);

      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, true);
      expect(await saveCompleter.future, equals(null));

      const resultDate = Duration(hours: 50, minutes: 12);
      await tester.enterText(inputFinder, '50,2');
      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(invalidTimeErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, true);
      expect(saveCompleter.isCompleted, true);
      expect(await changeCompleter.future, equals(resultDate));
      expect(await saveCompleter.future, equals(resultDate));
    });

    testWidgets('with initial value', (tester) async {
      final formKey = GlobalKey<FormState>();
      final inputFieldKey = GlobalKey();
      const inputFieldLabel = 'date';
      const requiredError = 'required';
      var changeCompleter = Completer<Duration?>();
      var saveCompleter = Completer<Duration?>();
      const initialValue = Duration(hours: 5, minutes: 13);

      await tester.pumpWidget(
        MaterialApp(
          locale: const Locale('de', 'DE'),
          supportedLocales: const [Locale('de', 'DE')],
          localizationsDelegates: const [
            ...GlobalMaterialLocalizations.delegates,
            ...FSWidgetLocalizations.localizationsDelegates,
            FSIntlOverrides.delegate,
          ],
          home: Scaffold(
            body: Form(
              key: formKey,
              child: FSTimeDurationFormField(
                key: inputFieldKey,
                decoration: const InputDecoration(labelText: inputFieldLabel),
                initialValue: initialValue,
                validator: (value) {
                  if (value == null) {
                    return requiredError;
                  } else {
                    return null;
                  }
                },
                onChanged: (value) {
                  if (changeCompleter.isCompleted) {
                    changeCompleter = Completer<Duration?>();
                  }
                  changeCompleter.complete(value);
                },
                onSaved: (value) {
                  if (saveCompleter.isCompleted) {
                    saveCompleter = Completer<Duration?>();
                  }
                  saveCompleter.complete(value);
                },
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();

      final labelFinder = find.text(inputFieldLabel);
      final requiredErrorFinder = find.text(requiredError);
      final inputFinder = find.byKey(inputFieldKey);
      final invalidTimeErrorFinder =
          find.text(FSWidgetLocalizationsDe().timeFormFieldInvalidFormat);

      expect(inputFinder, findsOneWidget);
      expect(labelFinder, findsOneWidget);
      expect(requiredErrorFinder, findsNothing);
      expect(invalidTimeErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, false);

      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, true);
      expect(await saveCompleter.future, equals(initialValue));

      const resultDate = Duration(hours: 2, minutes: 1);
      await tester.enterText(inputFinder, '2:01');
      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(invalidTimeErrorFinder, findsNothing);
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, true);
      expect(saveCompleter.isCompleted, true);
      expect(await changeCompleter.future, equals(resultDate));
      expect(await saveCompleter.future, equals(resultDate));
    });

    testWidgets('no onChange with focus loss', (tester) async {
      final formKey = GlobalKey<FormState>();
      final inputFieldKey = GlobalKey();
      final focusNode = FocusNode();
      const inputFieldLabel = 'date';
      const requiredError = 'required';
      var changeCompleter = Completer<Duration?>();
      var saveCompleter = Completer<Duration?>();
      const initialValue = Duration(hours: 5, minutes: 13);

      await tester.pumpWidget(
        MaterialApp(
          locale: const Locale('de', 'DE'),
          supportedLocales: const [Locale('de', 'DE')],
          localizationsDelegates: const [
            ...GlobalMaterialLocalizations.delegates,
            ...FSWidgetLocalizations.localizationsDelegates,
            FSIntlOverrides.delegate,
          ],
          home: Scaffold(
            body: Form(
              key: formKey,
              child: FSTimeDurationFormField(
                key: inputFieldKey,
                focusNode: focusNode,
                decoration: const InputDecoration(labelText: inputFieldLabel),
                initialValue: initialValue,
                validator: (value) {
                  if (value == null) {
                    return requiredError;
                  } else {
                    return null;
                  }
                },
                onChanged: (value) {
                  if (changeCompleter.isCompleted) {
                    changeCompleter = Completer<Duration?>();
                  }
                  changeCompleter.complete(value);
                },
                onSaved: (value) {
                  if (saveCompleter.isCompleted) {
                    saveCompleter = Completer<Duration?>();
                  }
                  saveCompleter.complete(value);
                },
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();

      final labelFinder = find.text(inputFieldLabel);
      final requiredErrorFinder = find.text(requiredError);
      final inputFinder = find.byKey(inputFieldKey);
      final invalidTimeErrorFinder =
          find.text(FSWidgetLocalizationsDe().timeFormFieldInvalidFormat);

      expect(inputFinder, findsOneWidget);
      expect(labelFinder, findsOneWidget);
      expect(requiredErrorFinder, findsNothing);
      expect(invalidTimeErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, false);

      formKey.currentState?.save();
      expect(formKey.currentState?.validate(), true);
      await tester.pumpAndSettle();
      expect(requiredErrorFinder, findsNothing);
      expect(changeCompleter.isCompleted, false);
      expect(saveCompleter.isCompleted, true);
      expect(await saveCompleter.future, equals(initialValue));

      expect(focusNode.hasPrimaryFocus, false);
      focusNode.requestFocus();
      await tester.pumpAndSettle();
      expect(focusNode.hasPrimaryFocus, true);
      focusNode.unfocus();
      await tester.pumpAndSettle();
      expect(focusNode.hasPrimaryFocus, false);
      expect(changeCompleter.isCompleted, false);
    });
  });
}
