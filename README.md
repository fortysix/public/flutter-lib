### adding dependencies to project

add the following under dependencies in the projects pubspec.yaml file
```
fs_core:
    hosted: https://unpub.fortysix.world
    version: ^0.3.1
```

### setup unpub documentation of GIONA

https://dart.dev/tools/pub/custom-package-repositories

***Uses MongoDB v. 5.0.0

***IMPORTANT: this package was made for arm64/v8 so to run it and set it up you need to run these commands:
- docker run --rm --privileged multiarch/qemu-user-static:register
- (If on Debain:) apt-get install qemu binfmt-support qemu-user-static (otherwise download it from here:) https://github.com/multiarch/qemu-user-static/releases
- (Share the file in the docker compose:) ./qemu-aarch64-static:/usr/bin/qemu-aarch64-static
- Link to reddit: https://www.reddit.com/r/docker/comments/c75uhq/how_to_run_arm64_containers_from_amd64_hosts_and/

***Volume needs to be set to - ./unpub-data/unpub-packages:/app/unpub-packages

***Unpub tool needed (downloaded and added to PATH variable):
- dart pub global activate unpub_auth

***For publishing Oauth is needed (unpub_auth login) (login with google)
***For publishing token is needed (unpub_auth get | dart pub token add <self-hosted-pub-server>)
***If user changes, the token has to be relaoded with: unpub_auth get

for information over the tool: https://github.com/bytedance/unpub

for info over uploading and authentication: https://github.com/bytedance/unpub/tree/master/unpub_auth


###### Docker compose:

version: "3.8"

services:
mongodb:
image: mongo:5.0
restart: always
networks:
- proxy-net
unpub:
container_name: unpub-amd64
image: trevorwang/unpub
platform: linux/arm64/v8
restart: always
ports:
- "4000:4000"
volumes:
- ./quemu-aarch64-static:/usr/bin/quemu-aarch64-static
- ./unpub/unpub-packages:/app/unpub-packages
networks:
- proxy-net
links:
- mongodb
depends_on:
- mongodb
environment:
DB_URL: mongodb://mongodb/pub
WAIT_HOSTS: mongodb
entrypoint: /app/entrypoint.sh


networks:
proxy-net:
name: proxy-net