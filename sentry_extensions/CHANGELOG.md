## 0.9.0

* update flutter_sentry to ^8.7.0

## 0.8.2

* minor fix
 
## 0.8.1

* differentiate before send log depending on exceptions present or not 

## 0.8.0

* updated min dart sdk to 3.0.0
* updated min flutter version to 3.10.0
* updated dependencies

## 0.7.1

* set release to SENTRY_RELEASE if setReleaseByEnv is true

## 0.7.0

* publication on unpub.fortysix.world

## 0.6.2

* replace init setting setRelease with setReleaseByEnv

## 0.6.1

* bump sentry_flutter to 7.8.0
* add init option for enabling tracing and for setting release version

## 0.6.0

* bump sentry_flutter to 7.4.0
 
## 0.5.1

* no changes, only for problems with renaming
 
## 0.5.0

* updated fs_lints and fs_core gitlab url

## 0.4.0

* upgrade to flutter 3.3

## 0.3.3

* add debug logs to before send in debug mode
 
## 0.3.2

* drop all events if not in release mode
* track Flutter.onError events

## 0.3.1

* update core
 
## 0.3.0

* update to flutter 3
* use internal lints

## 0.2.0

* force setting environment

## 0.1.1

* minor fix

## 0.1.0

* force setting version

## 0.0.8

* undeprecate set base url

## 0.0.7

* allow clearing of debug info by setting null

## 0.0.6

* add setDebugInfo
* deprecate similar methods

## 0.0.5

* minor typo fix

## 0.0.4

* allow setting infos from InfoController
* update to core version 0.0.3

## 0.0.3

* make error logger init function static
* introduce CustomEventProcessor for processing events before sending them to sentry

## 0.0.2

* update to core version 0.0.2

## 0.0.1

* initial sentry error logger implementation
