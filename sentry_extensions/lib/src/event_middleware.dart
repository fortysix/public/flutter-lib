import 'dart:async';

import 'package:sentry_flutter/sentry_flutter.dart';

typedef CustomEventProcessor = Future<SentryEvent?> Function(
  SentryEvent event, {
  dynamic hint,
});

class MiddlewareEventProcessor extends EventProcessor {
  MiddlewareEventProcessor(this.processor);

  final CustomEventProcessor processor;

  @override
  FutureOr<SentryEvent?> apply(SentryEvent event, Hint hint) async {
    return await processor(event, hint: hint);
  }
}
