import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:fs_core/fs_core.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

import 'event_middleware.dart';

class SentryController extends ErrorLogger {
  final List<CustomEventProcessor> middlewares;
  final Map<String, String> debugInfos = {};

  SentryController({
    List<CustomEventProcessor>? middlewares,
  })  : middlewares = middlewares ?? const [],
        super() {
    Sentry.configureScope((scope) {
      for (var element in this.middlewares) {
        scope.addEventProcessor(MiddlewareEventProcessor(element));
      }
    });

    FlutterError.onError = (FlutterErrorDetails details) {
      FlutterError.presentError(details);
      addBreadcrumb(
          message: 'FlutterErrorDetails',
          severity: BreadcrumbSeverity.warning,
          category: 'FlutterErrorDetails',
          data: {
            'context': details.context?.value,
            'exception': details.exception,
            'toString(debug)': details.toString(minLevel: DiagnosticLevel.debug),
            'stack': details.stack,
          });
      reportError(details, details.stack);
    };
  }

  static Future<void> init({
    required String dns,
    required AppRunner appRunner,
    required String packageName,
    required String version,
    required String buildNumber,
    required String environment,
    double? tracesSampleRate,

    /// set this to use release via build command --dart-define SENTRY_RELEASE=1.0.0
    bool setReleaseByEnv = false,
  }) async {
    return await SentryFlutter.init(
      (SentryFlutterOptions options) {
        options.dsn = dns;
        options.beforeSend = (event, dynamic hint) {
          if (kReleaseMode) {
            return event;
          } else {
            if (event.exceptions?.isNotEmpty ?? false) {
              debugPrint('[BEFORE_SEND] Sentry event WITH exceptions');
            } else {
              debugPrint('[BEFORE_SEND] Sentry event with NO exceptions');
            }
            return null;
          }
        };
        if (setReleaseByEnv) {
          options.release = const String.fromEnvironment('SENTRY_RELEASE');
        } else {
          // same as sentry_dart_plugin uses when uploading source maps
          options.release = '$packageName@$version+$buildNumber';
        }
        options.environment = environment;
        options.tracesSampleRate = tracesSampleRate;
      },
      appRunner: appRunner,
    );
  }

  @override
  void reportError(dynamic error, StackTrace? stackTrace) {
    Sentry.captureException(
      error,
      stackTrace: stackTrace,
    );
  }

  @override
  void addBreadcrumb({
    required String message,
    required category,
    String? type,
    Map<String, dynamic>? data,
    BreadcrumbSeverity? severity,
  }) {
    try {
      SentryLevel level;
      switch (severity) {
        case BreadcrumbSeverity.debug:
          level = SentryLevel.debug;
          break;
        case null:
        case BreadcrumbSeverity.info:
          level = SentryLevel.info;
          break;
        case BreadcrumbSeverity.warning:
          level = SentryLevel.warning;
          break;
        case BreadcrumbSeverity.error:
          level = SentryLevel.error;
          break;
        case BreadcrumbSeverity.fatal:
          level = SentryLevel.fatal;
          break;
      }

      if (kReleaseMode) {
        Sentry.addBreadcrumb(
          Breadcrumb(
            message: message,
            type: type,
            category: category,
            data: data,
            level: level,
          ),
        );
      } else {
        // ignore: avoid_print
        print(
          '[BREADCRUMB] $type $category: $message, ${data.toString()}',
        );
      }
    } catch (e) {
      // ignore: avoid_print
      print('Sending breadcrumb to sentry.io failed: $e');
    }
  }

  void setDebugInfo(String key, String? value) {
    if (value == null) {
      debugInfos.remove(key);

      Sentry.configureScope((scope) {
        scope.removeTag(key);
        scope.setContexts('Custom Infos', debugInfos);
      });
    } else {
      debugInfos[key] = value;

      Sentry.configureScope((scope) {
        scope.setTag(key, value);
        scope.setContexts('Custom Infos', debugInfos);
      });
    }
  }

  void setBaseUrl() {
    if (kIsWeb) {
      final host = Uri.base.host;
      if (host.trim().isNotEmpty) {
        Sentry.configureScope((scope) {
          scope.setTag('hostDomain', Uri.base.host);
          scope.setTag('baseUrl', Uri.base.toString());
        });
      }
    }
  }

  @Deprecated('Use [setDebugInfo] instead')
  void setApiBaseUrl(String baseURL) {
    Sentry.configureScope((scope) {
      scope.setTag('apiBaseUrl', baseURL);
    });
  }

  @Deprecated('Use [setDebugInfo] instead')
  void setDeviceToken(String value) {
    Sentry.configureScope(
      (scope) {
        scope.setTag('Device-Token', value);
        scope.setContexts('Device-Token', value);
      },
    );
  }

  @Deprecated('Use [setDebugInfo] instead')
  void setFirebaseUid(String value) {
    Sentry.configureScope(
      (scope) {
        scope.setTag('Firebase-UUID', value);
        scope.setContexts('Firebase-UUID', value);
      },
    );
  }

  @Deprecated('Use [setDebugInfo] instead')
  void setInfos({
    String? deviceName,
    bool? isVirtualDevice,
    bool? isWeb,
    bool? isMobile,
    String? appVersion,
  }) {
    Sentry.configureScope(
      (scope) {
        final Map<String, dynamic> infos = {};
        if (deviceName != null) {
          infos['Device Name'] = deviceName;
          scope.setTag('Device Name', deviceName);
        }
        if (isVirtualDevice != null) {
          infos['Is Virtual Device'] = isVirtualDevice;
          scope.setTag('Is Virtual Device', isVirtualDevice.toString());
        }
        if (isWeb != null) {
          infos['Is Web'] = isWeb;
          scope.setTag('Is Web', isWeb.toString());
        }
        if (isMobile != null) {
          infos['Is Mobile'] = isMobile;
          scope.setTag('Is Mobile', isMobile.toString());
        }
        if (appVersion != null) {
          infos['App Version'] = appVersion;
          scope.setTag('App Version', appVersion);
        }

        if (infos.isNotEmpty) {
          scope.setContexts('Custom Infos', infos);
        }
      },
    );
  }
}
