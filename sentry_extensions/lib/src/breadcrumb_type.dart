enum SentryBreadcrumbType {
  http,
  debug,
}

extension BreadcrumTypesExtension on SentryBreadcrumbType {
  String get value {
    switch (this) {
      case SentryBreadcrumbType.http:
        return 'http';
      case SentryBreadcrumbType.debug:
        return 'debug';
    }
  }
}
