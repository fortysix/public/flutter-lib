import 'package:dio/dio.dart';

class ErrorHandled {
  final bool handled;
  final Response<dynamic>? response;

  const ErrorHandled(this.handled, [this.response]);
}
