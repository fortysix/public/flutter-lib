import '../dio_extensions.dart';

abstract class ErrorHandler {
  const ErrorHandler();

  Future<ErrorHandled> handle(ApiException exception) async {
    throw UnimplementedError();
  }
}
