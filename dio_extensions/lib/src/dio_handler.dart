import 'package:dio/dio.dart';
import 'package:fs_core/fs_core.dart';

import 'error_handler.dart';
import 'exceptions.dart';
import 'types.dart';

class DioHandler {
  DioHandler({
    BaseOptions? options,
    List<Interceptor> interceptors = const <Interceptor>[],
    this.errorHandler = const <ErrorHandler>[],
    required this.onUnknownError,
  })  : dio = Dio(options),
        super() {
    dio.interceptors.addAll(interceptors);
  }

  final Dio dio;
  final List<ErrorHandler> errorHandler;
  final ErrorCallback onUnknownError;
  final Map<String, DateTime> lastDispatch = {};

  Future<Response<T?>?> callApi<T>(
    DioRequest<T> nativeApiCall, {
    List<ErrorHandler>? errorHandler,
    String? groupKey,
  }) async {
    Response<T?>? result;
    final dispatchTime = DateTime.now();
    try {
      result = await nativeApiCall();
    } on Exception catch (error, stackTrace) {
      result = await _handleException<T>(
          error, stackTrace, errorHandler ?? this.errorHandler);
    } catch (error, stackTrace) {
      onUnknownError(error, stackTrace);
    } finally {
      if (groupKey != null) {
        if (!lastDispatch.containsKey(groupKey) ||
            lastDispatch[groupKey]!.isBefore(dispatchTime)) {
          lastDispatch[groupKey] = dispatchTime;
        } else {
          throw GroupCanceledException(groupKey);
        }
      }
    }
    return result;
  }

  /// [groupKey] groups calls together and throws [GroupCanceledException] if a later dispatched call has finished first
  /// this is for example useful for search requests that get fired multiple times while user is typing
  /// this way the result for 'filter' gets ignored if 'filtered by text' finishes first
  Future<T?> callApiOrNull<T>(
    DioRequest<T> nativeApiCall, {
    List<ErrorHandler>? errorHandler,
    String? groupKey,
  }) async {
    try {
      final response = await callApi(
        nativeApiCall,
        errorHandler: errorHandler,
        groupKey: groupKey,
      );
      return response?.data;
    } catch (e) {
      return null;
    }
  }

  /// [groupKey] groups calls together and throws [GroupCanceledException] if a later dispatched call has finished first
  /// this is for example useful for search requests that get fired multiple times while user is typing
  /// this way the result for 'filter' gets ignored if 'filtered by text' finishes first
  Future<T?> callApiOrThrow<T>(
    DioRequest<T> nativeApiCall, {
    List<ErrorHandler>? errorHandler,
    String? groupKey,
  }) async {
    final response = await callApi(
      nativeApiCall,
      errorHandler: errorHandler,
      groupKey: groupKey,
    );
    return response?.data;
  }

  /// [groupKey] groups calls together and return null if a later dispatched call has finished first
  /// this is for example useful for search requests that get fired multiple times while user is typing
  /// this way the result for 'filter' gets ignored if 'filtered by text' finishes first
  Future<Response<T?>?> _handleException<T>(
    Exception exception,
    StackTrace stackTrace,
    List<ErrorHandler> errorHandler,
  ) async {
    final apiException = ApiException(
      originalError: exception,
      stackTrace: stackTrace,
      onUnknownError: onUnknownError,
    );
    try {
      for (var handler in errorHandler) {
        final result = await handler.handle(apiException);
        if (result.handled) {
          if (result.response != null &&
              result.response?.data?.runtimeType is T) {
            return result.response as Response<T>;
          } else {
            throw HandledException(exception, errorHandler, handler);
          }
        }
      }

      throw UnhandledException(exception, errorHandler);
    } on HandledException catch (_) {
      rethrow;
    } catch (error, stackTrace) {
      onUnknownError(error, stackTrace);
      rethrow;
    }
  }
}
