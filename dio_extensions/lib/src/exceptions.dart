import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:fs_core/fs_core.dart';

import '../dio_extensions.dart';

typedef ApiExceptionCallback = Future<ErrorHandled> Function(
    ApiException error);

class ApiException {
  final Exception originalError;
  final ErrorCallback onUnknownError;
  final StackTrace stackTrace;

  ApiException({
    required this.originalError,
    required this.stackTrace,
    required this.onUnknownError,
  });

  Future<Map> _getDataMap() async {
    if (!isDioException) {
      return {};
    }

    final dioException = originalError as DioException;
    if (dioException.response?.data == null) {
      return {};
    } else if (dioException.response!.data is String) {
      try {
        return jsonDecode(dioException.response!.data);
      } catch (_) {
        return {
          'plainTextResponse': dioException.response!.data,
        };
      }
    } else if (dioException.response!.data is Map) {
      return dioException.response!.data;
    } else if (dioException.response!.data is Uint8List) {
      final string = String.fromCharCodes(dioException.response!.data);
      return jsonDecode(string);
    } else if (dioException.response!.data.stream != null &&
        dioException.response!.data.stream is Stream<Uint8List>) {
      final uInt8 = await streamToUInt8List(
          dioException.response!.data.stream, onUnknownError);
      if (uInt8 != null) {
        final string = String.fromCharCodes(uInt8);
        return jsonDecode(string);
      }
    } else {
      throw Exception('Unknown ApiError data type');
    }
    return {};
  }

  Map? _dataCached;
  Future<Map> get dataMap async {
    _dataCached ??= await _getDataMap();
    return _dataCached!;
  }

  DioException? get dioException =>
      originalError is DioException ? originalError as DioException : null;
  bool get isDioException => dioException != null;
  Future<bool> get isApiError async => (await key) != null;

  Future<String?> get key async => (await dataMap)['Key'];
  Future<String?> get defaultMessage async => (await dataMap)['DefaultMessage'];

  static Future<Uint8List?> streamToUInt8List(
      Stream<Uint8List> stream, ErrorCallback onUnknownError) {
    final Completer<Uint8List?> completer = Completer<Uint8List?>();
    final intList = <int>[];
    StreamSubscription? subscription;
    subscription = stream.listen(
      (data) {
        subscription?.pause();
        intList.addAll(data.toList());
        subscription?.resume();
      },
      onDone: () async {
        try {
          completer.complete(Uint8List.fromList(intList));
        } catch (error, stackTrace) {
          onUnknownError(error, stackTrace);
          completer.complete();
        }
      },
      onError: (error, stackTrace) async {
        onUnknownError(error, stackTrace);
        completer.complete();
        throw error;
      },
      cancelOnError: true,
    );
    return completer.future;
  }
}

class HandledException implements Exception {
  HandledException(this.originalException, this.errorHandler, this.handler);

  final Object originalException;
  final List<ErrorHandler> errorHandler;
  final ErrorHandler handler;
}

class UnhandledException implements Exception {
  UnhandledException(this.originalException, this.errorHandler);

  final Object originalException;
  final List<ErrorHandler> errorHandler;
}

class GroupCanceledException implements Exception {
  GroupCanceledException(this.groupKey);

  final String groupKey;
}
