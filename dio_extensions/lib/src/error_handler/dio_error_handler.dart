import '../error_handled.dart';
import '../error_handler.dart';
import '../exceptions.dart';

class DioErrorHandler extends ErrorHandler {
  const DioErrorHandler(this.onDioError);

  final ApiExceptionCallback onDioError;

  @override
  Future<ErrorHandled> handle(ApiException exception) async {
    if (exception.isDioException) {
      return await onDioError(exception);
    } else {
      return const ErrorHandled(false);
    }
  }
}
