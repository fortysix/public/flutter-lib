import '../error_handled.dart';
import '../exceptions.dart';
import 'dio_error_handler.dart';

class UnauthenticatedException implements Exception {}

class UnauthenticatedErrorHandler extends DioErrorHandler {
  UnauthenticatedErrorHandler(this.onUnauthenticatedError)
      : super(
          (ApiException error) async {
            if (error.dioException?.response?.statusCode == 401) {
              return await onUnauthenticatedError(error);
            } else {
              return const ErrorHandled(false);
            }
          },
        );

  final ApiExceptionCallback onUnauthenticatedError;
}
