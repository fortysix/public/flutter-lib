import '../error_handled.dart';
import '../exceptions.dart';
import 'dio_error_handler.dart';

class ApiErrorHandler extends DioErrorHandler {
  ApiErrorHandler(this.onApiError)
      : super((ApiException exception) async {
          if (await exception.isApiError) {
            return await onApiError(exception);
          } else {
            return const ErrorHandled(false);
          }
        });

  final ApiExceptionCallback onApiError;
}
