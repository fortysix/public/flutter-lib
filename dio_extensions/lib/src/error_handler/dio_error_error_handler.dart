import 'package:dio/dio.dart';

import '../error_handled.dart';
import '../exceptions.dart';
import 'dio_error_handler.dart';

typedef DioErrorErrorCallback<T> = Future<ErrorHandled> Function(
  T error,
  StackTrace stackTrace,
);

class DioErrorErrorHandler<T> extends DioErrorHandler {
  DioErrorErrorHandler(this.onTypeError)
      : super(
          (ApiException exception) async {
            if (exception.dioException?.type == DioExceptionType.unknown &&
                exception.dioException?.error is T) {
              return await onTypeError(
                  exception.dioException?.error as T, exception.stackTrace);
            } else {
              return const ErrorHandled(false);
            }
          },
        );

  final DioErrorErrorCallback<T> onTypeError;
}
