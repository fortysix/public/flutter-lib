import '../error_handled.dart';
import '../exceptions.dart';
import 'dio_error_handler.dart';

class UnauthorizedException implements Exception {}

class UnauthorizedErrorHandler extends DioErrorHandler {
  UnauthorizedErrorHandler(this.onUnauthorizedError)
      : super(
          (ApiException error) async {
            if (error.dioException?.response?.statusCode == 403) {
              return await onUnauthorizedError(error);
            } else {
              return const ErrorHandled(false);
            }
          },
        );

  final ApiExceptionCallback onUnauthorizedError;
}
