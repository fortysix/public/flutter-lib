import 'dart:io';

import 'package:dio/dio.dart';

import '../error_handled.dart';
import '../exceptions.dart';
import 'dio_error_handler.dart';

class ServerNotReachedException implements Exception {}

class ConnectionErrorHandler extends DioErrorHandler {
  ConnectionErrorHandler(this.onConnectionError)
      : super(
          (ApiException exception) async {
            if (_isConnectionError(exception) ||
                _isConnectionTimeout(exception) ||
                _isReceiveTimeout(exception) ||
                _isSendTimeout(exception) ||
                _isSocketConnectionException(exception) ||
                _isHttpConnectionError(exception) ||
                _isXMLHttpRequestError(exception) ||
                _isHandshakeException(exception)) {
              return await onConnectionError(exception);
            } else {
              return const ErrorHandled(false);
            }
          },
        );

  final ApiExceptionCallback onConnectionError;

  static bool _isConnectionError(ApiException error) =>
      error.dioException?.type == DioExceptionType.connectionError;
  static bool _isConnectionTimeout(ApiException error) =>
      error.dioException?.type == DioExceptionType.connectionTimeout;
  static bool _isReceiveTimeout(ApiException error) =>
      error.dioException?.type == DioExceptionType.receiveTimeout;
  static bool _isSendTimeout(ApiException error) =>
      error.dioException?.type == DioExceptionType.sendTimeout;
  static bool _isSocketConnectionException(ApiException error) =>
      error.dioException?.error is SocketException &&
      [
        -2, // Failed host lookup (OS Error: Name or service not known, errno = -2)
        7, // No address associated with hostname
        8, // Failed host lookup
        9, // Bad file descriptor
        13, // Connection failed (OS Error: Permission denied, errno = 13)
        60, // Operation timed out
        65, // Connection failed (OS Error: No route to host, errno = 65)
        101, // SocketException: Network is unreachable (OS Error: Network is unreachable, errno = 101)
        103, // Software caused connection abort
        104, // Connection reset by peer
        111, // Connection refused
        113, // No route to host
        11001, // Failed host lookup (OS Error: Der angegebene Host ist unbekannt., errno = 11001)
      ].contains(
          (error.dioException?.error as SocketException).osError?.errorCode);
  static bool _isHttpConnectionError(ApiException error) =>
      error.dioException?.error is HttpException &&
      error.dioException?.message != null &&
      (error.dioException!.message!
              .startsWith('HttpException: Software caused connection abort,') ||
          error.dioException!.message!.startsWith(
              'HttpException: Connection closed while receiving data,'));
  // On web if no connection is available (exp.: in flight mode) this is the resulting error
  // no exacter information can be extracted out of the error
  // this is the same behavior on the dart httpClient so it is not dio specific
  // this has been cross tested to work on chrome, firefox, edge and safari on android, ios, windows, mac and linux
  // according to search result this also happens for CORS errors
  static bool _isXMLHttpRequestError(ApiException error) =>
      error.dioException?.error == 'XMLHttpRequest error.';
  static _isHandshakeException(ApiException error) =>
      error.dioException?.error is HandshakeException;
}
