import 'package:dio/dio.dart';

import '../types.dart';

class StartLoadingInterceptor extends Interceptor {
  StartLoadingInterceptor({required this.onLoadingStart});

  final LoadingStartCallback onLoadingStart;

  static const String _suppressHeaderKey = 'NO_LOADING';
  static Map<String, String> suppressLoadingHeader = {
    _suppressHeaderKey: 'true',
  };
  static const String _delayedHeaderKey = 'DELAYED_LOADING';
  static Map<String, String> delayedLoadingHeader = {
    _delayedHeaderKey: 'true',
  };
  static const String _tagHeaderKey = 'LOADING_TAG';
  static Map<String, String> tagLoadingHeader(String tag) => {
        _tagHeaderKey: tag,
      };

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    if (options.headers.containsKey(_suppressHeaderKey)) {
      options.headers.remove(_suppressHeaderKey);
    } else if (options.headers.containsKey(_tagHeaderKey)) {
      onLoadingStart(
        options.uri.toString(),
        tag: options.headers[_tagHeaderKey],
        delayed: options.headers.containsKey(_delayedHeaderKey),
      );
      options.headers.remove(_delayedHeaderKey);
    } else {
      onLoadingStart(
        options.uri.toString(),
        delayed: options.headers.containsKey(_delayedHeaderKey),
      );
      options.headers.remove(_delayedHeaderKey);
    }
    handler.next(options);
  }
}
