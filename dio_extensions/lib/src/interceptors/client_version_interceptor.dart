import 'package:dio/dio.dart';

import '../types.dart';

class ClientVersionInterceptor extends Interceptor {
  ClientVersionInterceptor({required this.getAppVersion});

  final AppVersionGetter getAppVersion;

  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    options.headers['Client-Version'] = await getAppVersion();
    handler.next(options);
  }
}
