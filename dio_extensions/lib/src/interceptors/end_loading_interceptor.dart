import 'package:dio/dio.dart';

import '../types.dart';

class EndLoadingInterceptor extends Interceptor {
  EndLoadingInterceptor({required this.onEndLoading});

  final LoadingEndCallback onEndLoading;

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    onEndLoading(response.requestOptions.uri.toString());
    handler.next(response);
  }

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    final RequestOptions requestOptions = err.requestOptions;
    onEndLoading(requestOptions.uri.toString());
    handler.next(err);
  }
}
