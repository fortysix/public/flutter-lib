import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

class DeviceTokenInterceptor extends Interceptor {
  static const String _deviceTokenKey = '__FS_DEVICE_TOKEN__';

  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    options.headers['Device-Token'] = await deviceToken;
    handler.next(options);
  }

  String? _deviceToken;
  Future<String> get deviceToken async {
    await _prepareDeviceToken();
    return _deviceToken!;
  }

  Future<void> _prepareDeviceToken() async {
    if (_deviceToken == null) {
      final prefs = await SharedPreferences.getInstance();
      var tempToken = prefs.getString(_deviceTokenKey);

      if (tempToken == null) {
        tempToken = const Uuid().v4();
        prefs.setString(_deviceTokenKey, tempToken);
      }

      _deviceToken = tempToken;
    }
  }
}
