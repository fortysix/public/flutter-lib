import 'package:dio/dio.dart';

import '../types.dart';

class AuthHeaderInterceptor extends Interceptor {
  AuthHeaderInterceptor({required this.getAuthToken});

  final AuthTokenGetter getAuthToken;

  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    final idToken = await getAuthToken();
    if (idToken != null) {
      options.headers['Authorization'] = 'Bearer $idToken';
    }
    handler.next(options);
  }
}
