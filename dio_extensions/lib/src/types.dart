import 'package:dio/dio.dart';

typedef DioRequest<T> = Future<Response<T>> Function();

typedef LoadingStartCallback = void Function(
  String key, {
  String? tag,
  bool delayed,
});

typedef LoadingEndCallback = void Function(String key);

typedef AuthTokenGetter = Future<String?> Function();

typedef AppVersionGetter = Future<String> Function();
