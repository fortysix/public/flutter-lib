## 0.10.0

* parse errors once before sending to error handler to prevent multiple reads of streams in response
* fix analyzer errors

## 0.9.0

* updated min dart sdk to 3.0.0
* updated min flutter version to 3.10.0
* updated dependencies

## 0.8.0

* publication on unpub.fortysix.world

## 0.7.2

* export HandledException

## 0.7.1

* catch DioErrorType.connectionError as offline error
* add 101 SocketException to list of offline errors

## 0.7.0

* actually throw error when using callApiOrThrow

## 0.6.1

* no changes, only for problems with renaming
 
## 0.6.0

* updated fs_core and fs_lint gitlab url

## 0.5.3

* export dio Interceptor, Response, ResponseInterceptorHandler, ErrorInterceptorHandler and DioErrorType
 
## 0.5.2

* export dio DioError
 
## 0.5.1

* export dio BaseOptions and Header
 
## 0.5.0

* rename to fs_dio_extensions
* updated dio to 5.0.0
* updated other dependencies to latest
 
## 0.4.5

* ApiErrorHandler: parse Uint8List data response
* DioHandler: catch errors of error handlers
 
## 0.4.4

* allow grouping of api calls and ignore out dated results of group
 
## 0.4.3

* make dataMap of ApiError public
 
## 0.4.2

* handle socket exception: 11001 Failed host lookup

## 0.4.1

* catch json parse errors in ApiError
* handle socket exception: -2 Name or service not known

## 0.4.0

* add onUnknownError callback to error handler 
* add api error handler

## 0.3.0

* do not return on interceptors

## 0.2.1

(CHANGES FROM 0.1.6)
* handle socket exception: 9 Bad file descriptor
* handle socket exception: 111 Connection refused

## 0.2.0

* update to flutter 3
* use internal lints

## 0.1.6

* handle socket exception: 9 Bad file descriptor
* handle socket exception: 111 Connection refused

## 0.1.5

* catch "Software caused connection abort" error in connection error handler
 
## 0.1.4

* fix custom error handlers not being passes on from callApiOrNull and callApiOrThrow

## 0.1.3

* minor bug fix

## 0.1.2

* fix offline detection for web

## 0.1.1

* unauthenticated and unauthorized error handler implemented

## 0.1.0

* rewrite of dio handler
* error handler for dio errors and for connection problems implemented

## 0.0.4

* fix device token interceptor resulting in null error
* expose DioErrorHandler

## 0.0.3

* add interceptors for:
    - auth header
    - client version
    - device token
    - start and end loading

## 0.0.2

* use core definition of ErrorCallback

## 0.0.1

* initial dio handler implementation
