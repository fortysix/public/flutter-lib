## 1.0.4

* allow use of environment variables

## 1.0.3

* disable diagnostic_describe_all_properties
 
## 1.0.2

* disable docs

## 1.0.1

* disable prefer_int_literals
 
## 1.0.0

* added all available lint rules (state 2024-03-27)

## 0.4.0

* updated min dart sdk to 3.0.0
* updated min flutter version to 3.10.0
* updated dependencies

## 0.3.0

* publication on unpub.fortysix.world
* avoid_redundant_argument_values
* rename main file to fs_lints.dart

## 0.2.0

* renamed package to fs_lints

## 0.1.0

* move flutter_lints from dev dependencies to dependencies 
* rename to fortysix_lints to prevent name collision on pub get

## 0.0.1

* prefer_final_locals
* prefer_relative_imports
* lines_longer_than_80_chars false