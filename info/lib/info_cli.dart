import 'package:file/local.dart';
import 'package:yaml/yaml.dart';

dynamic _getPubspec() {
  final file = const LocalFileSystem().file("pubspec.yaml");
  if (!file.existsSync()) {
    // ignore: avoid_print
    print("Pubspec not found: ${file.absolute.path}");
    return {};
  }
  final pubspecString = file.readAsStringSync();
  final pubspec = loadYaml(pubspecString);
  return pubspec;
}

String? getVersion() {
  try {
    final pubspec = _getPubspec();
    final versionString = pubspec['version'] as String?;
    if (versionString == null) {
      return null;
    }
    final versionSplit = versionString.split('+');
    return versionSplit.first;
  } catch (_) {
    return null;
  }
}
