import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/foundation.dart' show kIsWeb, TargetPlatform, defaultTargetPlatform;
import 'package:fs_core/fs_core.dart' show ErrorCallback;
import 'package:http/http.dart';
import 'package:package_info_plus/package_info_plus.dart';

class FSInfoProvider {
  const FSInfoProvider({this.onUnknownError});

  final ErrorCallback? onUnknownError;

  FutureOr<PackageInfo> get _platformInfo async {
    var packageInfo = PackageInfo(
      appName: 'unknown',
      packageName: 'unknown',
      version: '0.0.0',
      buildNumber: '0',
    );
    try {
      packageInfo = await PackageInfo.fromPlatform();
    } on ClientException catch (error, stackTrace) {
      if (error.message != 'XMLHttpRequest error.') {
        if (onUnknownError != null) {
          onUnknownError!(error, stackTrace);
        }
      }
    } catch (error, stackTrace) {
      if (onUnknownError != null) {
        onUnknownError!(error, stackTrace);
      }
    }
    return packageInfo;
  }

  Future<String> get appVersion async {
    const fsVersion = String.fromEnvironment('FS_INFO_VERSION');
    return fsVersion.isEmpty ? (await _platformInfo).version : fsVersion;
  }

  Future<String> get buildNumber async => (await _platformInfo).buildNumber;

  String get targetPlatform => defaultTargetPlatform.name;

  bool get isWeb => kIsWeb;

  bool get isAndroid => defaultTargetPlatform == TargetPlatform.android;

  bool get isIOS => defaultTargetPlatform == TargetPlatform.iOS;

  bool get isWindows => defaultTargetPlatform == TargetPlatform.windows;

  bool get isLinux => defaultTargetPlatform == TargetPlatform.linux;

  bool get isMac => defaultTargetPlatform == TargetPlatform.macOS;

  bool get isMobile => isAndroid || isIOS;

  Future<bool> get isVirtualDevice async {
    try {
      final deviceInfo = DeviceInfoPlugin();

      if (kIsWeb) {
        return false;
      }

      if (isAndroid) {
        final androidInfo = await deviceInfo.androidInfo;
        return !androidInfo.isPhysicalDevice;
      }

      if (isIOS) {
        final iosInfo = await deviceInfo.iosInfo;
        return !iosInfo.isPhysicalDevice;
      }

      if (isWindows) {
        // currently no way to check if virtual
        return false;
      }

      if (isLinux) {
        // currently no way to check if virtual
        return false;
      }

      if (isMac) {
        // currently no way to check if virtual
        return false;
      }

      return true;
    } catch (error, stackTrace) {
      if (onUnknownError != null) {
        onUnknownError!(error, stackTrace);

        return true;
      } else {
        rethrow;
      }
    }
  }

  Future<String> get deviceName async {
    try {
      final deviceInfo = DeviceInfoPlugin();

      if (kIsWeb) {
        final webBrowserInfo = await deviceInfo.webBrowserInfo;
        return webBrowserInfo.userAgent ?? 'no_user_agent_found';
      }

      if (isAndroid) {
        final androidInfo = await deviceInfo.androidInfo;
        return '${androidInfo.brand} - ${androidInfo.model}';
      }

      if (isIOS) {
        final iosInfo = await deviceInfo.iosInfo;
        return iosInfo.name;
      }

      if (isWindows) {
        final windowsInfo = await deviceInfo.windowsInfo;
        return windowsInfo.computerName;
      }

      if (isLinux) {
        final linuxInfo = await deviceInfo.linuxInfo;
        return linuxInfo.name;
      }

      if (isMac) {
        final macInfo = await deviceInfo.macOsInfo;
        return macInfo.computerName;
      }

      return 'fallback_device_name';
    } catch (error, stackTrace) {
      if (onUnknownError != null) {
        onUnknownError!(error, stackTrace);
        return 'error_fallback_name';
      } else {
        rethrow;
      }
    }
  }

  Future<String?> get osVersion async {
    try {
      final deviceInfo = DeviceInfoPlugin();

      if (kIsWeb) {
        return null;
      }

      if (isAndroid) {
        final androidInfo = await deviceInfo.androidInfo;
        return androidInfo.version.release;
      }

      if (isIOS) {
        final iosInfo = await deviceInfo.iosInfo;
        return iosInfo.systemVersion;
      }

      if (isWindows) {
        final windowsInfo = await deviceInfo.windowsInfo;
        return '${windowsInfo.majorVersion}|${windowsInfo.minorVersion}:${windowsInfo.servicePackMajor}|${windowsInfo.servicePackMinor}[${windowsInfo.buildNumber}]';
      }

      if (isLinux) {
        final linuxInfo = await deviceInfo.linuxInfo;
        return linuxInfo.version;
      }

      if (isMac) {
        final macInfo = await deviceInfo.macOsInfo;
        return macInfo.osRelease;
      }

      return null;
    } catch (error, stackTrace) {
      if (onUnknownError != null) {
        onUnknownError!(error, stackTrace);
        return 'error_fallback_os_version';
      } else {
        rethrow;
      }
    }
  }

  Future<String?> get osName async {
    try {
      final deviceInfo = DeviceInfoPlugin();

      if (kIsWeb) {
        final webBrowserInfo = await deviceInfo.webBrowserInfo;
        return webBrowserInfo.browserName.name;
      }

      if (isAndroid) {
        return defaultTargetPlatform.name;
      }

      if (isIOS) {
        final iosInfo = await deviceInfo.iosInfo;
        return iosInfo.systemName;
      }

      if (isWindows) {
        final windowsInfo = await deviceInfo.windowsInfo;
        return windowsInfo.productName;
      }

      if (isLinux) {
        final linuxInfo = await deviceInfo.linuxInfo;
        return linuxInfo.prettyName;
      }

      if (isMac) {
        final macInfo = await deviceInfo.macOsInfo;
        return macInfo.hostName;
      }

      return null;
    } catch (error, stackTrace) {
      if (onUnknownError != null) {
        onUnknownError!(error, stackTrace);
        return 'error_fallback_os_name';
      } else {
        rethrow;
      }
    }
  }

  Future<String?> get manufacturer async {
    try {
      final deviceInfo = DeviceInfoPlugin();

      if (kIsWeb) {
        final webBrowserInfo = await deviceInfo.webBrowserInfo;
        return webBrowserInfo.vendor;
      }

      if (isAndroid) {
        final androidInfo = await deviceInfo.androidInfo;
        return androidInfo.manufacturer;
      }

      if (isIOS) {
        return 'Apple';
      }

      if (isWindows) {
        return 'Microsoft';
      }

      if (isLinux) {
        return null;
      }

      if (isMac) {
        return 'Apple';
      }

      return null;
    } catch (error, stackTrace) {
      if (onUnknownError != null) {
        onUnknownError!(error, stackTrace);
        return 'error_fallback_manufacturer';
      } else {
        rethrow;
      }
    }
  }

  Future<String?> get model async {
    try {
      final deviceInfo = DeviceInfoPlugin();

      if (kIsWeb) {
        return null;
      }

      if (isAndroid) {
        final androidInfo = await deviceInfo.androidInfo;
        return androidInfo.model;
      }

      if (isIOS) {
        final iosInfo = await deviceInfo.iosInfo;
        return iosInfo.utsname.machine;
      }

      if (isWindows) {
        return null;
      }

      if (isLinux) {
        return null;
      }

      if (isMac) {
        final macInfo = await deviceInfo.macOsInfo;
        return macInfo.model;
      }

      return null;
    } catch (error, stackTrace) {
      if (onUnknownError != null) {
        onUnknownError!(error, stackTrace);
        return 'error_fallback_model';
      } else {
        rethrow;
      }
    }
  }

  Future<String?> get securityPatch async {
    try {
      final deviceInfo = DeviceInfoPlugin();

      if (kIsWeb) {
        return null;
      }

      if (isAndroid) {
        final androidInfo = await deviceInfo.androidInfo;
        return androidInfo.version.securityPatch;
      }

      if (isIOS) {
        return null;
      }

      if (isWindows) {
        return null;
      }

      if (isLinux) {
        return null;
      }

      if (isMac) {
        return null;
      }

      return null;
    } catch (error, stackTrace) {
      if (onUnknownError != null) {
        onUnknownError!(error, stackTrace);
        return 'error_fallback_security-patch';
      } else {
        rethrow;
      }
    }
  }

  Future<String?> get sdk async {
    try {
      final deviceInfo = DeviceInfoPlugin();

      if (kIsWeb) {
        return null;
      }

      if (isAndroid) {
        final androidInfo = await deviceInfo.androidInfo;
        return androidInfo.version.sdkInt.toString();
      }

      if (isIOS) {
        return null;
      }

      if (isWindows) {
        return null;
      }

      if (isLinux) {
        return null;
      }

      if (isMac) {
        return null;
      }

      return null;
    } catch (error, stackTrace) {
      if (onUnknownError != null) {
        onUnknownError!(error, stackTrace);
        return 'error_fallback_sdk';
      } else {
        rethrow;
      }
    }
  }

  Future<Locale?> get language async {
    try {
      final deviceInfo = DeviceInfoPlugin();

      if (kIsWeb) {
        final webBrowserInfo = await deviceInfo.webBrowserInfo;
        if (webBrowserInfo.language != null) {
          return _stringToLocale(webBrowserInfo.language!);
        } else {
          return null;
        }
      } else {
        return _stringToLocale(Platform.localeName);
      }
    } catch (error, stackTrace) {
      if (onUnknownError != null) {
        onUnknownError!(error, stackTrace);
        return null;
      } else {
        rethrow;
      }
    }
  }

  Locale? _stringToLocale(String localeName) {
    final splitLocaleName = localeName.split('.');
    if (splitLocaleName.isEmpty) {
      return null;
    }

    final splitLocale = splitLocaleName.first.split('_');
    if (splitLocale.isEmpty) {
      return null;
    }

    late Locale platformLocale;
    if (splitLocale.length == 2) {
      platformLocale = Locale(splitLocale.first, splitLocale[1]);
    } else {
      platformLocale = Locale(splitLocale.first);
    }
    return platformLocale;
  }
}
