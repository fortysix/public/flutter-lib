# 2.0.2

* file downgroade to play with flutter 3.24.3

# 2.0.1

* fix publishing and minor lint fix

# 2.0.0

* set min flutter version to 3.22.3
* update dependencies

## 1.1.0

* add language info

## 1.0.0

* update min dart sdk to 3.3.0
* update min flutter version to 3.19.0
* update dependencies to support wasm

## 0.6.0

* updated min dart sdk to 3.0.0
* updated min flutter version to 3.10.0
* updated dependencies

## 0.5.0

* publication on unpub.fortysix.world

## 0.4.8

* 0.4.7 did not have branch merged; this is the actual implementation
* add targetPlatform, osVersion, osName, manufacturer, model, securityPatch and sdk info

## 0.4.7

* add targetPlatform, osVersion, osName, manufacturer, model, securityPatch and sdk info

## 0.4.6

* fix FS_INFO_VERSION reading

## 0.4.5

* user environment variable FS_INFO_VERSION for version with fallback to current behaviour

## 0.4.4

* add cli to extract version number form pubspec.yaml

## 0.4.3

* add fallback package info
* handle package info loading issues on web 

## 0.4.2

* add isAndroid isIOS isWindows isLinux isMac

## 0.4.1

* no changes, only for problems with renaming
 
## 0.4.0

* updated fs_lints and fs_core gitlab url

## 0.3.0

* renames package to fs_info
* rename InfoProvider to FSInfoProvider
* update dependencies to latest
* extend FSInfoProvider to windows, linux and mac

## 0.2.0

* upgrade to flutter 3.3
* update dependencies to latest

## 0.1.0

* update to flutter 3
* use internal lints

## 0.0.2

* add buildNumber

## 0.0.1

* initial info provider implementation
