enum BreadcrumbSeverity {
  debug,
  info,
  warning,
  error,
  fatal,
}

abstract class ErrorLogger {
  void reportError(dynamic error, StackTrace stackTrace);

  void addBreadcrumb({
    required String message,
    required category,
    String? type,
    Map<String, dynamic>? data,
    BreadcrumbSeverity? severity,
  });
}
