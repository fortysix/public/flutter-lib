## 0.4.0

* updated min dart sdk to 3.0.0
* updated min flutter version to 3.10.0

## 0.3.0

* use fs_lints unpub version

## 0.3.1

* publication on unpub.fortysix.world

## 0.2.0

* renamed package to fs_core
* updated fs_lint gitlab url

## 0.1.0

* update to flutter 3
* use internal lints

## 0.0.3

* universal configuration of web url path strategy

## 0.0.2

* Add error callback type

## 0.0.1

* Abstract ErrorLogger defined 
